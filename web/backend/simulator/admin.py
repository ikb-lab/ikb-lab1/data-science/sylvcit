from django.contrib import admin
from simulator.models import Tree, Candidates
from import_export import resources
from import_export.admin import ImportExportModelAdmin


# class TreeAdmin(admin.ModelAdmin):
#     pass

class TreeResource(resources.ModelResource):
    class Meta:
        model = Tree

class TreeAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    # fields = ('latin_name', 'french_name', 'english_name')
    list_display = ('latin_name', 'french_name', 'english_name')
    search_fields = ('latin_name', 'french_name', 'english_name')

    resource_class = TreeResource

class CandidatesResource(resources.ModelResource):
    class Meta:
        model = Candidates

class CandidatesAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    # fields = ('latin_name', 'french_name', 'english_name')
    list_display = ('specie_latin', 'specie_french', 'specie_english')
    search_fields = ('specie_latin', 'specie_french', 'specie_english')

    resource_class = CandidatesResource

# Register your models here.
# @admin.register(Tree)
admin.site.register(Tree, TreeAdmin)
admin.site.register(Candidates, CandidatesAdmin)