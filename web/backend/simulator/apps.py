from django.apps import AppConfig


class PlantingSimulatorConfig(AppConfig):
    name = 'simulator'
