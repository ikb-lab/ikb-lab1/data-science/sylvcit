from django.db import models
import hashlib

# Create your models here.
class Tree(models.Model):
    # id = models.AutoField(primary_key = True)
    mtl_code = models.CharField(max_length=255)
    latin_name = models.CharField(max_length=255)
    correct_latin_name = models.CharField(max_length=255, blank=True)
    species_latin_cleaned = models.CharField(max_length=255)
    genus = models.CharField(max_length=255)	
    family = models.CharField(max_length=255)	
    uqam_code = models.CharField(max_length=255)	
    func_group = models.CharField(max_length=255, blank=True)	
    sylvcit_id = models.CharField(max_length=128, blank=True, default=None)
    iTree_code = models.CharField(max_length=255, blank=True)
    synonyms = models.CharField(max_length=255,blank=True)
    isShrub = models.CharField(max_length=255, blank=True)	
    french_name	= models.CharField(max_length=255)
    english_name = models.CharField(max_length=255)
    isImputed = models.CharField(max_length=255, blank=True)	
    max_dbh = models.FloatField()
    def save(self, *args, **kwargs):
        if self.sylvcit_id is None:
            self.sylvcit_id = hashlib.sha256((self.correct_latin_name).encode('ISO8859-1')).hexdigest()
        super().save(*args, **kwargs)  # Call the "real" save() method.
    class Meta:
        verbose_name = "Arbre"

    def __str__(self):
        return self.latin_name

# Create your models here.
class Candidates(models.Model):
    code= models.CharField(max_length=255)
    specie_mtl= models.CharField(max_length=255)
    specie_french= models.CharField(max_length=255)
    specie_english= models.CharField(max_length=255)
    specie_latin= models.CharField(max_length=255)
    genus= models.CharField(max_length=255, blank=True)
    family= models.CharField(max_length=255, blank=True)
    uqam_code= models.CharField(max_length=255, blank=True)
    func_group= models.CharField(max_length=255, blank=True)
    Rue= models.CharField(max_length=255)
    Parc= models.CharField(max_length=255)
    condition= models.CharField(max_length=255)
    image_url= models.CharField(max_length=255)
    hq_details_url= models.CharField(max_length=255)
    class Meta:
        verbose_name = "Candidates"

    def __str__(self):
        return self.specie_latin