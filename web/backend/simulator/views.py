import logging

from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import NameForm, plantingForm
import pandas as pd
import random
import requests
import json
from app import recommender
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
import re
import pysolr
from colorhash import ColorHash
from bs4 import BeautifulSoup
import multiprocessing
from joblib import Parallel, delayed
from simulator.models import Tree, Candidates

num_cores = multiprocessing.cpu_count()

hq_base_url = 'https://arbres.hydroquebec.com/'

# Trefle Token
trefle_token = 'bf1u91bo95iOLuvzMb4iyDC7fV4k9AjooWIul5uw6ac'

# api-endpoint 
trefle_url = 'https://trefle.io/api/v1/plants/search?token='+trefle_token

# Get candidates
column_names = ['id','code', 'specie_mtl', 'specie_french', 'specie_english',
                 'specie_latin', 'genus', 'family', 'uqam_code', 'func_group', 'Rue', 'Parc', 'condition', 'image_url', 'hq_details_url']
# Lecture du fichier avec panda
candidates_df = pd.read_csv('../../data/candidates_new2.csv')
# candidates_df = pd.DataFrame(list(Candidates.objects.all().values()))
prices_trees_df = pd.read_csv('../../data/valeur_remplacement_clean_AS.csv')
# Recuperation des labels de colonnes
candidates_df.columns = column_names
# Mets un index au dataframe
candidates_df = candidates_df.set_index('specie_latin')

# By using ‘last’, the last occurrence of each set of duplicated values is set on False and all others on True:
candidates_df = candidates_df[~candidates_df.index.duplicated(keep='last')]
url_solr = "http://urbanforest_solr:8983"
solr_species_core = pysolr.Solr(url_solr+'/solr/species', always_commit=True)
solr_forest_core = pysolr.Solr(url_solr+'/solr/urban-forest', always_commit=True, results_cls=dict)
# Do a health check.
# print("solr ping************", solr_species_core.ping())
# print("solr ping************", solr_forest_core.ping())
ping_rsp = solr_species_core.ping()
ping_rsp = json.loads(ping_rsp) 
# print('ping response **********' , ping_rsp.get('status'))
# while json.loads(solr_species_core.ping()).status!='OK':
# Get species data
resp = solr_species_core.search('*:*', rows=1000, limit=1000)

# lecture de la donnee et mise de l'index a code
species_df = pd.DataFrame(resp.docs).set_index('code')
species_df = species_df[~species_df.index.duplicated(keep='last')]




# Get biomass data.
biomass_df = pd.read_csv('../../data/biomass_data.csv').set_index('uqam_code')

# Create new recommender. Instanciation de la classe et passage des dataframes au constructeur
rcm = recommender.recommender(candidates_df, species_df, biomass_df, prices_trees_df)


# Api, prends en params l'id du district
@api_view(['POST'])
def home(request):
    districts = ['Ahuntsic - Cartierville', 'Saint-Léonard', 'Ville-Marie', 'Côte-des-Neiges - Notre-Dame-de-Grâce',
                 'Verdun', 'Rosemont - La Petite-Patrie']

    dist_id = int(request.data["id"])
    district = districts[dist_id]
    params = {
        'q': 'district:{}'.format(district),
        'rows': 300000
    }

    resp = solr_forest_core.search('*:*', **params)
    trees = pd.DataFrame(resp['response']['docs'])
    resp = requests.get(url_solr+'/solr/urban-forest/select?', params=params)
    trees = pd.DataFrame(resp.json()['response']['docs'])

    rcm.define_area(trees)

    # Get current status of area.
    curr_status = rcm.get_status(trees)

    # Removed the null values from every column
    temp = curr_status['func_groups_list']
    # list comprehension, facon d'ecrire une boucle : creation d'une liste avec des valeurs non nulles
    curr_status['func_groups_list'] = [x for x in temp if x == x]

    temp = curr_status['species_list']
    curr_status['species_list'] = [x for x in temp if x == x]

    temp = curr_status['genus_list']
    curr_status['genus_list'] = [x for x in temp if x == x]

    temp = curr_status['families']
    curr_status['families'] = [x for x in temp if x == x]

    curr_status = curr_status.to_dict()

    # Render page.
    return Response({'district': district, 'curr_status': curr_status})


@api_view(['POST'])
def get_all_selected_trees(request):
    shape_coordinates = request.data["shape_coordinates"]
    # shape_type = request.data["shape_type"]
    # radius = request.data["radius"]
    # if shape_type == 'circle':
    #     params = {
    #         # 'q': '*:*',
    #         'fq': '{!geofilt sfield=coord}',
    #         'pt': shape_coordinates,
    #         'd': radius,
    #         'rows': 300000
    #     }
    # else:
    #     params = {
    #         # 'q': '*:*',
    #         'fq': '{!field f=coord}Intersects(' + format(shape_coordinates) + ')',
    #         'rows': 300000
    #     }

    params = {
            # 'q': '*:*',
            'fq': '{!field f=coord}Intersects(' + format(shape_coordinates) + ')',
            'rows': 300000
    }


    resp = solr_forest_core.search('*:*', **params)
    trees = pd.DataFrame(resp['response']['docs'])

    # trees.to_csv('selected_trees.csv')
    rcm.define_area(trees)

    # Get current status of area.
    curr_status = rcm.get_status(trees, species_df)
    # curr_status = rcm.get_status(trees)

    # Removed the null values from every column
    temp = curr_status['func_groups_list']
    curr_status['func_groups_list'] = [x for x in temp if x == x]

    temp = curr_status['species_list']
    curr_status['species_list'] = [x for x in temp if x == x]

    temp = curr_status['genus_list']
    curr_status['genus_list'] = [x for x in temp if x == x]

    temp = curr_status['families']
    curr_status['families'] = [x for x in temp if x == x]

    curr_status = curr_status.to_dict()
    curr_status["shape_coordinates"] = shape_coordinates
    # curr_status["shape_type"] = shape_type
    # curr_status["radius"] = radius
    # print(curr_status)
    # logging.warning(curr_status)
    # Render page.
    return Response({'curr_status': curr_status})


@api_view(['POST'])
def get_new_status(request):
    shape_coordinates = request.data["shape_coordinates"]
    # shape_type = request.data["shape_type"]
    # radius = request.data["radius"]
    list_new_species = request.data["list_new_species"]

    # if shape_type == 'circle':
    #     params = {
    #         # 'q': '*:*',
    #         'fq': '{!geofilt sfield=coord}',
    #         'pt': shape_coordinates,
    #         'd': radius,
    #         'rows': 300000
    #     }
    # else:
    #     params = {
    #         # 'q': '*:*',
    #         'fq': '{!field f=coord}Intersects(' + format(shape_coordinates) + ')',
    #         'rows': 300000
    #     }
    params = {
            # 'q': '*:*',
            'fq': '{!field f=coord}Intersects(' + format(shape_coordinates) + ')',
            'rows': 300000
    }

    # resp = requests.get(url_solr+'/solr/urban-forest/select?', params=params)
    # trees = pd.DataFrame(resp.json()['response']['docs'])

    resp = solr_forest_core.search('*:*', **params)
    trees = pd.DataFrame(resp['response']['docs'])

    rcm.define_area(trees)


    new_status, info_status = rcm.predict_status(trees, list_new_species)
    info_status = info_status.to_dict()
    new_status = new_status.to_dict()

    # print(new_status)
    # print("info_status")
    # print(info_status)
    return Response({'new_status': new_status, 'info_status': info_status})

@api_view(['POST'])
def get_trees(request):
    districts = ['Ahuntsic - Cartierville', 'Saint-Léonard', 'Ville-Marie', 'Côte-des-Neiges - Notre-Dame-de-Grâce',
                 'Verdun', 'Rosemont - La Petite-Patrie']

    dist_id = int(request.data["id"])
    district = districts[dist_id]
    params = {
        # 'q': '*:*',
        'q': 'district:{}'.format(district),
        'rows': 200
    }
    resp = solr_forest_core.search('*:*', **params)

    # Render page.
    return Response({'trees': resp})

def get_hq_details(species_name):
    try:
        url = "https://arbres.hydroquebec.com/resultat-arbres-arbustes?rechercheParNom="+species_name+"&type.id=37&plantationDistanceMinimum.id=9&zoneRusticite.id=42&forme.id=77&expositionLumiere.id=87&hauteur.id=57&solHumidite.id=92&largeur.id=67"
        respHQ = requests.get(url)
        # parse html
        page = BeautifulSoup(respHQ.content, features="html.parser")
        resultList = page.find("div", {"class": "l-resultat-item"})
        
        if resultList == -1:
            return 'no_data','no_data'
        else:
            figure = resultList.find("figure")
            if figure is not None :
                tree_image = figure.find("img").get('src',None)
                tree_details = figure.find("a").get('href',None)
                return hq_base_url+tree_image,hq_base_url+tree_details
            else:
                return 'no_data','no_data'
    except Exception as e:
        # print("exception")
        print(e)
        return 'no_data','no_data'




@api_view(['POST'])
def get_tree_detail(request):

    dist_id = int(request.data["tree_id"])
    # print(dist_id)
    params = {
        # 'q': '*:*',
        'fq': 'tree_id:{}'.format(dist_id),
        'fl':'specie_latin, specie_french, specie_latin_cleaned, specie_english, DBH, date_measures, date_plant, func_group, family, genus, uqam_code, coord',
        'rows': 5000
    }
    resp = solr_forest_core.search('*:*', **params)
    params2 = {
        # 'q': '*:*',
        'fq': 'code:{}'.format(resp['response']['docs'][0]['uqam_code']),
        'fl':'max_DBH, species_latin, code',
        'rows': 100
    }
    resp2 = solr_species_core.search('*:*', **params2)
    # print(resp2.raw_response['response']['docs'])
    if len(resp2.raw_response['response']['docs'])>0:
        resp['response']['docs'][0]['max_DBH'] = resp2.raw_response['response']['docs'][0]['max_DBH']
    # tree_info = resp2.json()
    # defining a params dict for the parameters to be sent to the API 
    resp['response']['docs'][0]['image_url'], resp['response']['docs'][0]['hq_details_url'] = get_hq_details(resp['response']['docs'][0]['specie_latin_cleaned'])

    # image_url
    # Render page.
    return JsonResponse(resp['response'])


@api_view(['POST'])
def get_by_bounds(request):
    bounds = request.data["bounds"]
    params = {
        # 'q': '*:*',
        # 'fl': 'specie_french, specie_latin, coord, func_group, district',
        'fl': 'specie_latin_cleaned, coord, tree_id, DBH',
        'fq': 'coord:{}'.format(bounds),
        'rows': 500000
    }

    # c = ColorHash('Hello World')
    # print(c.hex)

    # Get species data
    resp = solr_forest_core.search('*:*', **params)
    # print(resp['response'])
    for tree in resp['response']['docs']:
        # print(tree['specie_latin_cleaned'])
        # print((ColorHash(tree['specie_latin_cleaned'])).hex)
        tree['color_specie'] = (ColorHash(tree['specie_latin_cleaned'])).hex
    # Render page.
    return JsonResponse(resp['response'])


@api_view(['POST'])
def get_selected_trees_details(request):
    polygon_coordinates = request.data["polygon_coordinates"]

    resp = solr_forest_core.search('*:*', **{
    'fl': 'DBH, uqam_code',
    'fq': '{!field f=coord}Intersects(' + format(polygon_coordinates) + ')',
    'rows': 500000
    })


    trees = pd.DataFrame(resp['response']['docs'])
    # trees.to_csv('selected_area.csv')

    if len(trees) == 0:
        curr_status = pd.Series()
        curr_status['number_trees'] = 0
        curr_status['nb_old_trees'] = 0
    else:
        df_merged = pd.merge(trees, species_df[['uqam_code', 'new_max_DBH']], right_on='code', left_on='uqam_code',
                             how='left')
        curr_status = rcm.get_status_selected_trees(df_merged)

    curr_status = curr_status.to_dict()
    # print(curr_status)
    # Render page.
    return JsonResponse(curr_status)


@api_view(['POST'])
def get_selected_trees_details_circle(request):
    radius = request.data["radius"]
    latlngs = request.data["latlngs"]

    resp_trees = solr_forest_core.search('*:*', **{
        'fl': 'specie_french, district, DBH, uqam_code, date_measures, date_plant',
        'fq': '{!geofilt sfield=coord}',
        'pt': latlngs,
        'd': radius,
        'rows': 5000
    })
    trees = pd.DataFrame(resp_trees['response']['docs'])


    if len(trees) == 0:
        curr_status = pd.Series()
        curr_status['number_trees'] = 0
        curr_status['nb_old_trees'] = 0
    else:
        df_merged = pd.merge(trees, species_df[['uqam_code', 'new_max_DBH']], right_on='code', left_on='uqam_code',
                             how='inner')
        # df_merged.to_csv("df_merged", sep='\t', encoding='utf-8')
        # Write the DataFrame to JSON (as easy as can be)
        json = df_merged.to_json(orient='index')  # output just the records (no fieldnames) as a collection of tuples
        # Get current status of area.
        curr_status = rcm.get_status_selected_trees(df_merged)

    curr_status = curr_status.to_dict()
    # Render page.
    return Response({'trees': curr_status})


@api_view(['GET'])
def get_all_trees(request):
    params = {
        'q': '*:*',
        'fl': 'specie_latin_cleaned, coord, tree_id, DBH',
        # 'q': 'district:{}'.format(district),
        'rows': 300000
    }
    resp = requests.get(url_solr+'/solr/urban-forest/select?', params=params)
    resp_parsed = re.sub(r'^jsonp\d+\(|\)\s+$', '', resp.text)
    data = json.loads(resp_parsed)

    # trees = resp.json()['response']['docs']
    # logging.warning(data)

    # Render page.
    return Response({'trees': data})


# Index page
def index(request):
    print('Page actuelle acceuil')
    form = NameForm()
    return render(request, "simulator/index.html", {'form': form})


# Current status page
def curr_status(request):

    form = NameForm(request.GET)
    if form.is_valid():
        district = form.cleaned_data['district'][0]
    else:
        return render(request, "simulator/404.html", {'message': 'Error! Invalid data.'})

    # Retrieve trees of area.
    params = {
        'q': 'district:{}'.format(district),
        'rows': 300000
    }
    resp = requests.get(url_solr+'/solr/urban-forest/select?', params=params)

    trees = pd.DataFrame(resp.json()['response']['docs'])
    rcm.define_area(trees)

    # Get current status of area.
    curr_status = rcm.get_status(trees).to_dict()

    # Render page.
    return render(request, "simulator/status.html", {'district': district, 'curr_status': curr_status})


# Planting settings page.
def planting_settings(request):
    form = plantingForm()
    return render(request, "simulator/planting_settings.html", {'form': form})


# Selection of groups page.
def select_groups(request):
    # Get weights for features.
    w1 = float(request.GET['species_diversity_range'])
    w2 = float(request.GET['species_richness_range'])
    w3 = float(request.GET['group_diversity_range'])
    w4 = float(request.GET['group_richness_range'])
    w5 = float(request.GET['carbon_storage_range'])
    w6 = float(request.GET['value_range'])

    # Selection of groups page.
    trees = rcm.trees_set_dataframe
    if len(trees) == 0:
        return redirect('/')

    # Extract features from area.
    features_matrix = rcm.features_extractor(trees)

    # Get ranking of recommendations.
    rcm.recommender(features_matrix, w1, w2, w3, w4, w5, w6)
    ranking = rcm.ranking.values.tolist()

    # Get number of plants to be planted.
    form = plantingForm(request.GET)
    if form.is_valid():
        num_plants = form.cleaned_data['num_plants']
    else:
        return render(request, "simulator/404.html", {'message': 'Error! Invalid data.'})

    # Get list of functional groups.
    func_groups = ['1A', '1B', '2A', '2B', '2C', '3A', '3B', '4A', '4B', '5']

    # Render page.
    return render(request, "simulator/select_groups.html",
                  {'num_plants': num_plants, 'ranking': ranking, 'func_groups': func_groups})


# Selection of groups page.
@api_view(['POST'])
def get_groups(request):
    # Get weights for features.
    w1 = float(request.data['species_diversity_range'])
    w2 = float(request.data['species_richness_range'])
    w3 = float(request.data['group_diversity_range'])
    w4 = float(request.data['group_richness_range'])
    w5 = float(request.data['carbon_storage_range'])
    w6 = float(request.data['value_range'])
    trees_location = request.data['trees_location']

    print('**************Group diversity range **********')
    print(w3)
    print(trees_location)


    num_plants = int(request.data['num_plants'])

    # Selection of groups page.
    trees = rcm.trees_set_dataframe
    # if len(trees) == 0:
    # return redirect('/')

    # Extract features from area.
    features_matrix = rcm.features_extractor(trees, trees_location)

    # Get ranking of recommendations.

    ranking = rcm.recommender(features_matrix, w1, w2, w3, w4, w5, w6)
    # Get list of functional groups.
    # func_groups = ['1A', '1B', '2A', '2B', '2C', '3A', '3B', '4A', '4B', '5']
    ranking = ranking[~ranking.isin([pd.np.nan, pd.np.inf, -pd.np.inf]).any(1)]
    ranking = ranking.round(2)
    percentage_groups = (ranking['func_group'].value_counts(normalize=True) * 100).round(2)
    # print(percentage_groups.to_dict())
    
    # ranking.to_csv("ranking.csv")
    # tranform to array of objects
    data = ranking.T.to_dict()
    data = list(data.values())

    # Get list of groups.
    group_list = ranking['func_group'].unique()
    """Converting a list to dictionary with list elements as keys in dictionary
        using dict.fromkeys()
    """
    dictOfgroups = dict.fromkeys(group_list , True)
    # Render page.
    return Response({'ranking': data, 'recommended_groups': dictOfgroups,'percentage_by_group' : percentage_groups.to_dict() ,'nb_trees_plant':num_plants})


def select_species(request):
    # Get set of trees.
    trees = rcm.trees_set_dataframe

    # Get list of selected func groups.
    groups = [x for x in request.GET.keys()]

    # Get ranking of recommendations.
    ranking = rcm.ranking
    ranking = ranking.loc[ranking['func_group'].isin(groups)]

    # Get list of species.
    species_list = ranking['species_latin'].tolist()
    if len(trees) == 0:
        return redirect('/')

    # Render page.
    return render(request, "simulator/select_species.html", {'groups': groups, 'species_list': species_list})

def get_hq_details2(species):
    species_name = species["species_latin"]
    try:
        url = "https://arbres.hydroquebec.com/resultat-arbres-arbustes?rechercheParNom="+species_name+"&type.id=37&plantationDistanceMinimum.id=9&zoneRusticite.id=42&forme.id=77&expositionLumiere.id=87&hauteur.id=57&solHumidite.id=92&largeur.id=67"
        respHQ = requests.get(url)
        # parse html
        page = BeautifulSoup(respHQ.content, features="html.parser")
        resultList = page.find("div", {"class": "l-resultat-item"})
        
        if resultList == -1:
            return 'no_data','no_data'
        else:
            figure = resultList.find("figure")
            if figure is not None :
                tree_image = figure.find("img").get('src',None)
                tree_details = figure.find("a").get('href',None)
                species['image_url'], species['hq_details_url'] = hq_base_url+tree_image, hq_base_url+tree_details
                # return hq_base_url+tree_image,hq_base_url+tree_details
            else:
                species['image_url'], species['hq_details_url'] = 'no_data','no_data'
    except Exception as e:
        # print("exception")
        print(e)
        species['image_url'], species['hq_details_url'] = 'no_data','no_data'
    return species


@api_view(['POST'])
def get_species_selected_groups(request):
    # Get set of trees.
    trees = rcm.trees_set_dataframe

    selected_groups = request.data['group_list']
    # Get list of selected func groups.
    groups = list(filter(selected_groups.get, selected_groups))

    # Get ranking of recommendations.
    ranking = rcm.ranking
    ranking = ranking[~ranking.isin([pd.np.nan, pd.np.inf, -pd.np.inf]).any(1)]
    ranking = ranking.round(2)
    ranking = ranking.loc[ranking['func_group'].isin(groups)]
    # print(ranking)
    # Get list of species.
    # species_list = ranking.values.tolist()
    species_list = ranking[['species_latin','func_group', 'condition', 'image_url', 'hq_details_url']].to_dict('records')
    # species_list = ranking['species_latin'].tolist()
    random.shuffle(species_list)
    # processed_list =Parallel(n_jobs=num_cores)(delayed(get_hq_details2)(species) for species in species_list)
    # print(processed_list)
    # for species in species_list:
    #     # print(species["species_latin"])
    #     species['image_url'], species['hq_details_url'] = get_hq_details(species["species_latin"])
    # # Render page.
    return Response({'groups': groups, 'species_list': species_list})


def results(request):
    # Get initial set of trees
    trees = rcm.trees_set_dataframe

    # Get current status of area.
    curr_status = rcm.get_status(trees).to_dict()
    return render(request, "simulator/results.html", {'curr_status': curr_status})
