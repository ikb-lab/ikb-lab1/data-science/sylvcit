"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path, include
from . import views
admin.site.site_header = 'SylvAdmin'                    # default: "Django Administration"
app_name = 'SylvCiT'
urlpatterns = [
    re_path(r'^$', views.index, name="index"),
    re_path(r'^current-status/', views.curr_status, name="curr_status"),
    re_path(r'^planting_settings/', views.planting_settings, name="planting_settings"),
    re_path(r'^select_groups/', views.select_groups, name="select_groups"),
    re_path(r'^api/get_groups', views.get_groups, name="get_groups"),
    re_path(r'^api/get_species_selected_groups', views.get_species_selected_groups, name="get_species_selected_groups"),
    re_path(r'^select_species/', views.select_species, name="select_species"),
    re_path(r'^results/', views.results, name="results"),
    re_path(r'^api/home', views.home, name="home"),
    re_path(r'^api/get_trees', views.get_trees, name="get_trees"),
    re_path(r'^api/get_tree_detail', views.get_tree_detail, name="get_tree_detail"),
    re_path(r'^api/get_all_trees', views.get_all_trees, name="get_all_trees"),
    re_path(r'^api/get_by_bounds', views.get_by_bounds, name="get_by_bounds"),
    re_path(r'^api/get_selected_trees_details', views.get_selected_trees_details, name="get_selected_trees_details"),
    re_path(r'^api/get_selected_circle', views.get_selected_trees_details_circle, name="get_selected_circle"),
    re_path(r'^api/get_all_selected_trees', views.get_all_selected_trees, name="get_all_selected_trees"),
    re_path(r'^api/get_new_status', views.get_new_status, name="get_new_status"),
    
]
