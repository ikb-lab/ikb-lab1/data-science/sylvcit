from rest_framework import serializers
from .models import Tree


# Serializers define the API representation.
class TreesSerializer(serializers.Serializer):
    class Meta:
        model = Tree