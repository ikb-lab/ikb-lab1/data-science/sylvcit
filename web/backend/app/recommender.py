import pandas as pd
import numpy as np
import math
from sklearn import preprocessing
import logging
from operator import itemgetter
import time

from app.constants import CARBON_COST


class recommender:

    def __init__(self, candidates_df, species_df, biomass_df, prices_trees_df):
        self.candidates_df = candidates_df
        self.species_df = species_df
        self.biomass_df = biomass_df
        self.trees_set_dataframe = pd.DataFrame()
        self.prices_trees_df = prices_trees_df
        self.ranking = pd.DataFrame()

    def area_effective_diversity_species(self, trees_set_dataframe):
        """Calculates the effective diversity of a given area in terms of species.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
            about trees in a specific area.
        Returns:
            A float number that represents the effective diversity.
        """
        import math
        sum_DIV = 0

        num_trees_per_species = trees_set_dataframe.groupby(['uqam_code'])['tree_id'].count()

        total_abundance = len(trees_set_dataframe)
        #total_abundance = len(trees_set_dataframe.index)  # plus performante
        # Iterates over the DataFrame columns, returning a tuple with the column name and the content
        for name, num_individuals in num_trees_per_species.items():
            DIV = num_individuals / total_abundance * math.log(num_individuals / total_abundance)
            sum_DIV += abs(DIV)
        return math.exp(sum_DIV)

    def area_species_richness(self, trees_set_dataframe):
        """Calculates the richness of a given area in terms of species diversity.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
            about trees in a specific area.
        Returns:
            A float number that represents the richness.
        """
        return trees_set_dataframe.uqam_code.nunique()

    def area_effective_diversity_groups(self, trees_set_dataframe):
        """Calculates the effective diversity of a given area in terms of functional groups.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
            A float number that represents the effective diversity.
        """
        import math
        sum_DIV = 0
        trees_set_dataframe = trees_set_dataframe[trees_set_dataframe.func_group != 'na']
        num_trees_per_species = trees_set_dataframe.groupby(['func_group'])['tree_id'].count()
        # total_abundance = len(trees_set_dataframe)
        total_abundance = len(trees_set_dataframe.index)  # plus performante
        for name, num_individuals in num_trees_per_species.items():
            DIV = num_individuals / total_abundance * math.log(num_individuals / total_abundance)
            sum_DIV += abs(DIV)

        return math.exp(sum_DIV)

    def improvement_effective_diversity_species(self, new_species, curr_effect_diversity, curr_richness,
                                                trees_set_dataframe):
        """Calculates the improvement that a new tree would cause in a given area,
            in terms of species effective diversity.
      
        Args:
            new_species: string that represents the name of the new species that is being added to the area.
            curr_effect_diversity: current effective diversity of the area (species).
            curr_richness: current richness of the area (species).
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area. 
        Returns:
            A float number that represents the improvement.
        """

        import math

        # If new group is not defined
        if type(new_species) != str:
            return 0
        total_abundance = len(trees_set_dataframe)
        num_individuals = trees_set_dataframe.groupby(['uqam_code'])['tree_id'].count()[new_species]
        if num_individuals == 1:
            previous_DIV = 0
        else:
            previous_DIV = abs(
                (num_individuals - 1) / (total_abundance - 1) * math.log((num_individuals - 1) / (total_abundance - 1)))
        DIV = abs(num_individuals / total_abundance * math.log(num_individuals / total_abundance))
        return math.exp(DIV - previous_DIV) - 1

    def improvement_richness_species(self, curr_richness, trees_set_dataframe):
        """Calculates the improvement that a new tree would cause in a given area,
            in terms of species richness.
      
        Args:
            curr_richness: current richness of the area (species).
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.         
        Returns:
            A float number that represents the improvement.
        """
        return (trees_set_dataframe.uqam_code.nunique() / curr_richness) - 1

    def area_groups_richness(self, trees_set_dataframe):
        """Calculates the richness of a given area in terms of functional groups diversity.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
            about trees in a specific area.
        Returns:
            A float number that represents the richness.
        """
        print("uniques groups****")
        m = trees_set_dataframe.func_group=='na'
        return trees_set_dataframe.func_group.mask(m).dropna().nunique()

    def improvement_richness_groups(self, curr_richness, trees_set_dataframe):
        """Calculates the improvement that a new tree would cause in a given area,
            in terms of functional group richness.
      
        Args:
            curr_richness: current richness of the area (groups).
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area. 
        Returns:
            A float number that represents the improvement.
        """
        return (trees_set_dataframe.func_group.nunique() / curr_richness) - 1

    def improvement_effective_diversity_groups(self, new_group, curr_effect_diversity, curr_richness,
                                               trees_set_dataframe):
        """Calculates the improvement that a new tree would cause in a given area,
            in terms of functional groups effective diversity.
      
        Args:
            new_group: string that represents the name of the new functional group that is beeing added to the area.
            curr_effect_diversity: current effective diversity of the area (groups).
            curr_richness: current richness of the area (groups).
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.         
        Returns:
            A float number that represents the improvement.
        """
        import math

        # If new group is not defined
        if type(new_group) != str:
            return 0
        total_abundance = len(trees_set_dataframe)
        num_individuals = trees_set_dataframe.groupby(['func_group'])['tree_id'].count()[new_group]
        if num_individuals == 1:
            previous_DIV = 0
        else:
            previous_DIV = abs(
                (num_individuals - 1) / (total_abundance - 1) * math.log((num_individuals - 1) / (total_abundance - 1)))
        DIV = abs(num_individuals / total_abundance * math.log(num_individuals / total_abundance))
        return math.exp(DIV - previous_DIV) - 1

    def violation_10_rule(self, trees_set_dataframe):
        """ identify species that doesn't follow 10-rule (number of trees 
            of such species corresponds to more than 10% of total).
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
            about trees in a specific area.
        Returns:
            A list of species(uqam_code) that violate the rule.
        """
        total = float(len(trees_set_dataframe))
        rule_results = trees_set_dataframe.groupby(['uqam_code'])['tree_id'].count().apply(
            lambda x: float(x) / total <= 0.1)
        violation_list = []
        for index, value in rule_results.items():
            if value == False:
                violation_list.append(index.lower())
        return violation_list

    def violation_20_rule(self, trees_set_dataframe):
        """ identify genuses that doesn't follow 20-rule (number of trees 
            of such genus corresponds to more than 20% of total).
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
            about trees in a specific area.
        Returns:
            A list of genus that violate the rule.
        """
        total = float(len(trees_set_dataframe))
        rule_results = trees_set_dataframe.groupby(['genus'])['tree_id'].count().apply(
            lambda x: float(x) / total <= 0.2)
        violation_list = []
        for index, value in rule_results.items():
            if value == False:
                violation_list.append(index.lower())
        return violation_list

    def get_percentage_family(self, tree_set_dataframe) :
        P = tree_set_dataframe.groupby('family')['tree_id'].count().nlargest(10).reset_index()
        df2 = tree_set_dataframe.groupby('family')['tree_id'].count().reset_index()
        combined = P.append(df2)
        combined =combined[~combined.index.duplicated(keep=False)]
        if combined['tree_id'].sum()> 0 :
            P_list=P.values.tolist()
            P_list.append(['Autres', combined['tree_id'].sum()])
            P = pd.DataFrame(P_list, columns=['family', 'tree_id'])
            P = P.sort_values(by='tree_id', ascending=False)
        P['Percentage'] = round((100 * P['tree_id']  / P['tree_id'].sum()),2)
        area_dict = dict(zip(P.family, P.Percentage))
        return area_dict

    def get_percentage_genus(self, tree_set_dataframe) :
        P = tree_set_dataframe.groupby('genus')['tree_id'].count().nlargest(10).reset_index()
        df2 = tree_set_dataframe.groupby('genus')['tree_id'].count().reset_index()
        combined = P.append(df2)
        combined =combined[~combined.index.duplicated(keep=False)]
        if combined['tree_id'].sum()> 0 :
            P_list=P.values.tolist()
            P_list.append(['Autres', combined['tree_id'].sum()])
            P = pd.DataFrame(P_list, columns=['genus', 'tree_id'])
            P = P.sort_values(by='tree_id', ascending=False)

        P['Percentage'] = round((100 * P['tree_id']  / P['tree_id'].sum()),2)
        area_dict = dict(zip(P.genus, P.Percentage))
        # print('area_dict')
        # print(area_dict)
        return area_dict

    def get_percentage_species(self, tree_set_dataframe) :
        P = tree_set_dataframe.groupby('specie_latin_cleaned')['tree_id'].count().nlargest(10).reset_index()
        df2 = tree_set_dataframe.groupby('specie_latin_cleaned')['tree_id'].count().reset_index()
        combined = P.append(df2)
        combined =combined[~combined.index.duplicated(keep=False)]
        if combined['tree_id'].sum()> 0 :
            P_list=P.values.tolist()       
            P_list.append(['Autres', combined['tree_id'].sum()])
            P = pd.DataFrame(P_list, columns=['specie_latin_cleaned', 'tree_id'])
            P = P.sort_values(by='tree_id', ascending=False)

        P['Percentage'] = round((100 * P['tree_id']  / P['tree_id'].sum()),2)
        area_dict = dict(zip(P.specie_latin_cleaned, P.Percentage))
        return area_dict
        
    def get_percentage_funcgroup(self, tree_set_dataframe) :
        P = tree_set_dataframe.groupby('specie_latin_cleaned')['tree_id'].count().nlargest(10).reset_index()
        P['Percentage'] = round((100 * P['tree_id']  / P['tree_id'].sum()),2)
        area_dict = dict(zip(P.specie_latin_cleaned, P.Percentage))
        return area_dict

    def violation_30_rule(self, trees_set_dataframe):
        """ identify families that doesn't follow 30-rule (number of trees 
            of such functionnal group corresponds to more than 30% of total).
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
            about trees in a specific area.
        Returns:
            A list of families that violate the rule.
        """
        total = float(len(trees_set_dataframe))
        rule_results = trees_set_dataframe.groupby(['func_group'])['tree_id'].count().apply(
            lambda x: float(x) / total <= 0.3)
        violation_list = []
        for index, value in rule_results.items():
            if value == False:
                violation_list.append(index.lower())
        return violation_list

    def biomass(self, uqam_code, DBH):
        """Calculates the biomass of a given species.
      
        Args:
            uqam_code: code that represents the species at UQAM.
            DBH: diameter of tree.

        Returns:
            A float number that represents the biomass.
        """
        tree_code_uqam = uqam_code
        evergreen =self.species_df.loc[self.species_df['uqam_code'] == tree_code_uqam, 'evergreen']
        if len(evergreen) > 0 :
            evergreen=evergreen.values[0]
        else :
            evergreen='noData'
        
        if uqam_code not in self.biomass_df.index.values:
            # print(uqam_code)
            if not pd.isnull(uqam_code) and uqam_code[:2] in self.biomass_df.index.values:
                uqam_code = uqam_code[:2]
            else :
                if evergreen == 'True':
                    uqam_code = 'SOFTWOOD'
                elif evergreen == 'False':
                    uqam_code = 'HARDWOOD'
                else:
                    uqam_code = 'ALL'

        bwood1 = self.biomass_df['bwood1'][uqam_code]
        bwood2 = self.biomass_df['bwood2'][uqam_code]
        ewood = 0

        bbark1 = self.biomass_df['bbark1'][uqam_code]
        bbark2 = self.biomass_df['bbark2'][uqam_code]
        ebark = 0

        bfoliage1 = self.biomass_df['bfoliage1'][uqam_code]
        bfoliage2 = self.biomass_df['bfoliage2'][uqam_code]
        efoliage = 0

        bbranches1 = self.biomass_df['bbranches1'][uqam_code]
        bbranches2 = self.biomass_df['bbranches2'][uqam_code]
        ebranches = 0

        biomass_wood = bwood1 * (DBH ** bwood2) + ewood
        biomass_bark = bbark1 * (DBH ** bbark2) + ebark
        biomass_foliage = bfoliage1 * (DBH ** bfoliage2) + efoliage
        biomass_branches = bbranches1 * (DBH ** bbranches2) + ebranches
        #Mise en commentaire le 18 02 2021
        # total_biomass = biomass_wood + biomass_bark + biomass_foliage + biomass_branches
        # Suppression du terme de la biomasse foliaire ()
        aerial_biomass = biomass_wood + biomass_bark + biomass_branches
        urban_aerial_biomass = aerial_biomass*0.8
        # #Calcul de la biomasse racinaire
        if evergreen == 'True':
            root_biomass = aerial_biomass*0.2222
        else :
            root_biomass = 1.576*(aerial_biomass)**0.615

        total_biomass =  urban_aerial_biomass + root_biomass
            # # Stockage de carbone en kg
            # carbon_storage = urban_total_biomass*0.5
            # # Calcul valeur carbone (Coût social)
            # carbon_value = carbon_storage*0.165
        return total_biomass

    def tree_value(self, uqam_code, DBH):
        """Calculates the value of a given species in terms of money.
      
        Args:
            uqam_code: code that represents the species at UQAM.
            DBH: diameter of tree.

        Returns:
            A float number that represents the value.
        """
        return CARBON_COST * self.carbon_storage(uqam_code, DBH)

    def total_value(self, trees_set_dataframe):
        """Calculates the total value of a given area.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
            A float number that represents the total value of the area.
        """
        result = 0
        for uqam_code, DBH in zip(trees_set_dataframe['uqam_code'], trees_set_dataframe['DBH']):
            if math.isnan(DBH):
                DBH = 53.42  # Mean value of max DBHs
            result += self.tree_value(uqam_code, DBH)
        return round(result,3)

    def value_by_species(self, trees_set_dataframe, all=False):
        """Calculates the total value of a given area.

        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
            A dictionary of total value by species
        """
        species = {}
        for uqam_code, DBH, specie_latin_cleaned in zip(trees_set_dataframe['uqam_code'], trees_set_dataframe['DBH'], trees_set_dataframe['specie_latin_cleaned']):
            if math.isnan(DBH):
                DBH = 53.42  # Mean value of max DBHs

            if (specie_latin_cleaned in species):
                species[specie_latin_cleaned] += self.tree_value(uqam_code, DBH)
            else:
                species[specie_latin_cleaned] = self.tree_value(uqam_code, DBH)
        species = self.round_dict_values(species, 3)
        species = dict(sorted(species.items(), key = itemgetter(1), reverse = True))
        if not all : 
            res = dict(sorted(species.items(), key = itemgetter(1), reverse = True)[:10]) 
            intersection = species.keys() & res.keys()
            for key in intersection:
                species.pop(key, None)
            res["Autres"] = sum(species.values())
            return res
        else:
            return species
        return species

    def replacement_value(self, species_name, uqam_code, DBH, condition_factor):
        """Calculates the ornementale value of a given species.
      
        Args:
            species_name: specie_name_cleaned
            DBH: diameter of tree.
        Returns:
            A float number that represents the ornementale value of such species.
        """
        status = "small"
        print("***********Begin replacement value************")
        print("species_name")
        print(species_name)
        
        condition_factor = condition_factor[0]
        print("condition factor")
        print(condition_factor)
        if condition_factor ==0:
            if "fraxinus" in species_name.lower():
                condition_factor = 0.35
            else:
                condition_factor = 0.65
        location_factor = 0.65
        tree_code_uqam = uqam_code
        evergreen =self.species_df.loc[self.species_df['uqam_code'] == tree_code_uqam, 'evergreen']
        print("evergreen")
        print(evergreen)
        if len(evergreen) > 0 :
            evergreen=evergreen.values[0]
        else :
            evergreen='unknown'
        # purchase_cost = self.prices_trees_df.loc[(self.prices_trees_df['species_latin_cleaned'] == species_name & self.prices_trees_df['dbh'] == DBH), 'purchase_cost']
        # print("round(DBH)")
        # print(round(DBH))
        purchase_cost = self.prices_trees_df.loc[(self.prices_trees_df['species_latin_cleaned'] == species_name) & (self.prices_trees_df['dbh'] == round(DBH)), 'purchase_cost']
        if len(purchase_cost) > 0 :
            print("if")
            purchase_cost=purchase_cost.values[0]
            status = "small"
        else:
            print("else")
            # get max dbh pepiniere
            max_dbh_nursery, species_factor, purchase_cost = self.prices_trees_df.loc[(self.prices_trees_df['species_latin_cleaned'] == species_name), ['dbh', 'species_factor', 'purchase_cost']].max()
            print("max_dbh_nursery")
            print(max_dbh_nursery)
            print("species_factor")
            print(species_factor)
            print("purchase_cost")
            print(purchase_cost)
            surface_terriere_max_dbh_pepiniere = max_dbh_nursery * max_dbh_nursery/4 * math.pi
            cout_unitaire_surface_terriere = purchase_cost / surface_terriere_max_dbh_pepiniere
            surface_terriere_arbre = DBH * DBH / 4 * math.pi
            diff_surf_terriere = surface_terriere_arbre - surface_terriere_max_dbh_pepiniere
            estimation_valeur_surface_terriere = diff_surf_terriere * cout_unitaire_surface_terriere
            estim_val_surf_ter_fact_species = estimation_valeur_surface_terriere * species_factor
            status = "big"
        print("purchase_cost")
        print(purchase_cost)
        if evergreen == 'True':
            replacement_cost = purchase_cost * 2.5
        elif evergreen== 'False' :
            replacement_cost = purchase_cost * 3
        else :
            replacement_cost = purchase_cost * 2.75
        # plant_cost = replacement_cost - purchase_cost
        if status is "small":
            estimation_with_condition = replacement_cost * condition_factor
            replacement_value = estimation_with_condition * location_factor 
        else:
            estim_with_repl_cost = replacement_cost + estim_val_surf_ter_fact_species
            estim_with_health = estim_with_repl_cost * condition_factor
            replacement_value = estim_with_health * location_factor
        print("***********END replacement value************")
        print(replacement_value)
        return 0 if math.isnan(replacement_value) else replacement_value

    def total_replacement_value(self, trees_set_dataframe):
        """Calculates the total replacement_value of a given area.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.

        Returns:
            A float number that represents the total replacement_value of the area.
        """
        result = 0
        # print(trees_set_dataframe['specie_latin_cleaned'])
        for specie_latin_cleaned,uqam_code, DBH, condition_factor in zip(trees_set_dataframe['specie_latin_cleaned'],trees_set_dataframe['uqam_code'], trees_set_dataframe['DBH'], trees_set_dataframe['health_condition_per']):
            # This method is used to check whether a given parameter is a valid number or not.
            if math.isnan(DBH):
                DBH = 53.42  # Mean value of max DBHs
            result += self.replacement_value(specie_latin_cleaned,uqam_code, DBH, condition_factor)
        return round(result,3)
    
    def replacement_value_by_species(self, trees_set_dataframe, all=False):
        """Calculates the total carbon storage of a given area.

        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.

        Returns:
            A dictionary of carbon storage by species
        """
        print("calculate replacement value by species")
        species = {}
        for specie_latin_cleaned,uqam_code, DBH, condition_factor in zip(trees_set_dataframe['specie_latin_cleaned'],trees_set_dataframe['uqam_code'], trees_set_dataframe['DBH'], trees_set_dataframe['health_condition_per']):
            # This method is used to check whether a given parameter is a valid number or not.
            if math.isnan(DBH):
                DBH = 53.42  # Mean value of max DBHs
            if (specie_latin_cleaned in species):
                species[specie_latin_cleaned] += self.replacement_value(specie_latin_cleaned,uqam_code, DBH, condition_factor)
            else:
                species[specie_latin_cleaned] = self.replacement_value(specie_latin_cleaned,uqam_code, DBH, condition_factor)
        species = self.round_dict_values(species, 3)
        species = dict(sorted(species.items(), key = itemgetter(1), reverse = True))
        print("Species dict")
        print(species)
        if not all : 
            res = dict(sorted(species.items(), key = itemgetter(1), reverse = True)[:10]) 
            intersection = species.keys() & res.keys()
            for key in intersection:
                species.pop(key, None)
            res["Autres"] = sum(species.values())
            return res
        else:
            return species

    def carbon_storage(self, uqam_code, DBH):
        """Calculates the carbon storage of a given species.
      
        Args:
            uqam_code: code that represents the species at UQAM.
            DBH: diameter of tree.
        Returns:
            A float number that represents the carbon storage of such species.
        """
        
        return 0.5 * self.biomass(uqam_code, DBH)

    def total_carbon_storage(self, trees_set_dataframe):
        """Calculates the total carbon storage of a given area.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.

        Returns:
            A float number that represents the total carbon storage of the area.
        """
        # print(trees_set_dataframe.columns)
        result = 0
        for uqam_code, DBH in zip(trees_set_dataframe['uqam_code'], trees_set_dataframe['DBH']):
            # This method is used to check whether a given parameter is a valid number or not.
            if math.isnan(DBH):
                DBH = 15  # Updated 07-07-2021 : 15 cms for dbh is a good compromise 
            result += self.carbon_storage(uqam_code, DBH)
        return round(result,3) 

    def carbon_storage_by_species(self, trees_set_dataframe, all=False):
        """Calculates the total carbon storage of a given area.

        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.

        Returns:
            A dictionary of carbon storage by species
        """
        species = {}
        # print("trees_set_dataframe")
        # print(trees_set_dataframe.columns)
        for uqam_code, DBH, specie_latin_cleaned in zip(trees_set_dataframe['uqam_code'], trees_set_dataframe['DBH'], trees_set_dataframe['specie_latin_cleaned']):
            
            # This method is used to check whether a given parameter is a valid number or not.
            if math.isnan(DBH):
                DBH = 15  # Updated 07-07-2021 : 15 cms for dbh is a good compromise 
            if (specie_latin_cleaned in species):
                species[specie_latin_cleaned] += self.carbon_storage(uqam_code, DBH)
            else:
                species[specie_latin_cleaned] = self.carbon_storage(uqam_code, DBH)
        species = self.round_dict_values(species, 3)
        species = dict(sorted(species.items(), key = itemgetter(1), reverse = True))
        if not all : 
            res = dict(sorted(species.items(), key = itemgetter(1), reverse = True)[:10]) 
            intersection = species.keys() & res.keys()
            for key in intersection:
                species.pop(key, None)
            res["Autres"] = sum(species.values())
            return res
        else:
            return species

    def improvement_carbon_storage(self, curr_total_storage, uqam_code, DBH):
        """Calculates the improvement that a new tree would cause in a given area,
            in terms of carbon storage.
      
        Args:
            uqam_code: code that represents the species at UQAM.
            DBH: usual diameter of trees from such species.
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
            A float number that represents the improvement.
        """
        return (self.carbon_storage(uqam_code, DBH) + curr_total_storage) / curr_total_storage - 1

    def improvement_value(self, curr_total_value, uqam_code, DBH):
        """Calculates the improvement that a new tree would cause in a given area,
            in terms of value.
      
        Args:
            uqam_code: code that represents the species at UQAM.
            DBH: usual diameter of trees from such species.
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
            A float number that represents the increase value.
        """
        return (self.tree_value(uqam_code, DBH) + curr_total_value) / curr_total_value - 1

    def improvement_ornemental_value(self, curr_ornemental_value, uqam_code, DBH):
        """Calculates the improvement that a new tree would cause in a given area,
            in terms of ornemental value.
      
        Args:
            uqam_code: code that represents the species at UQAM.
            DBH: usual diameter of trees from such species.
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
            A float number that represents the improvement.
        """
        return (self.carbon_storage(uqam_code, DBH) + curr_total_storage) / curr_total_storage - 1

    def score(self, diversity_improv_species, richness_improv_species, diversity_improv_groups, richness_improv_groups,
              carbon_storage, value, w1, w2, w3, w4, w5, w6):
        """Calculates the final score of a given specie based on how 
            good for the environment would be to plant a tree of such species.
      
        Args:
            diversity_improv: improvement that such species would have on diversity.
            richness_improv: improvement that such species would have on richness.
            carbon_storage: carbon storage of such species.
            value: tree value in terms of money.
            w1,w2,w3,w4: weights for above features.
        Returns:
            A float number that represents the score.
        """
        score = (w1 * diversity_improv_species + w2 * richness_improv_species + w3 * diversity_improv_groups
                 + w4 * richness_improv_groups + w5 * carbon_storage + w6 * value)
        return score

    def get_area_groups(self, trees_set_dataframe):
        """Get list of functional groups that are represented in a given area.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
           A list with the names of the functional groups.
        """
        # Initialisation d'une liste vide
        group_list = set()
        # Ajout des valeurs dans la liste
        for group in trees_set_dataframe['func_group']:
            group_list.add(group)
        # cleaned_group_list = [x for x in countries if str(x) != 'nan']
        return list({x for x in group_list if x==x})

    def get_old_trees(self, trees_set_dataframe, convertToDict=False):
        """Get list of old trees present in a given area.

        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
           number of old trees
        """
        """if (trees_set_dataframe['col1'].equals(trees_set_dataframe['col2'])):
            sdsd"""

        # storing boolean series in new
        # new = trees_set_dataframe["DBH"] >= trees_set_dataframe["max_DBH"]
        old_trees = trees_set_dataframe[trees_set_dataframe["DBH"] >= trees_set_dataframe["new_max_DBH"]]
        # print(old_trees)
        if convertToDict:
            # print(old_trees.columns)
            old_trees = old_trees[["specie_latin_cleaned", "specie_latin", "func_group","DBH", "new_max_DBH","date_measures", "date_plant", "coord"]]
            old_trees = old_trees.fillna('')
            old_trees = old_trees.to_dict('records')
        return old_trees
    
    def get_dbh_classes(self, trees_set_dataframe) :
        bins = [0, 10, 20, 30, 40, 50, 60, 70, 80]
        labels = ['<=10','11-20','21-30','31-40','41-50','51-60','61-70','70+']
        vc = pd.cut(trees_set_dataframe['DBH'], bins=bins, labels=labels).value_counts(normalize=True).mul(100).round(2)
        vc.index = vc.index.astype(str)
        test = vc
        # test.append(test.value_counts(normalize=True))
        d = vc.to_dict()
        # s = {k : d[k] for k in sorted(d)}
        tuple_list = d.items()
        sorted_data = sorted(tuple_list)
        sorted_data.insert(0, sorted_data.pop())
        s = dict(sorted_data)
        # print(s)
        return s

    def get_species_row(self, name):
        print("Get species row")
        print(name)
        print(self.candidates_df.columns)
        print(self.candidates_df.loc[name].name)
        return self.candidates_df.loc[name].name

    def get_group_row(self, name):
        return self.candidates_df.loc[name]['func_group']

    def get_family_row(self, name):
        return self.candidates_df.loc[name]['family']

    def get_genus_row(self, name):
        return self.candidates_df.loc[name]['genus']

    def get_condition_row(self, name):
        return self.candidates_df.loc[name]['condition']
    
    def get_details_url_row(self, name):
        return self.candidates_df.loc[name]['hq_details_url']

    def get_image_row(self, name):
        return self.candidates_df.loc[name]['image_url']
    
    def round_dict_values(self, dict_value, K):
        # print(dict_value)
        for k, v in dict_value.items():
            dict_value[k] = round(v, K)
        # print(dict_value)
        return dict_value

    def recommender(self, features, w1, w2, w3, w4, w5, w6):
        """Recommends species of trees that should be planted in a given area.
      
        Args:
            features: A Pandas DataFrame expected to contain features values
                of trees in a specific area.
            w1,w2,w3,w4,w5,w6: weights for features.
            trees_location : street or parks
        Returns:
           A pandas Dataframe that contains a ranking of recommended species.
        """
        # Normalize features values and calculate scores
        index = features.index.values
        features_matrix = preprocessing.scale(features[['carbon_storage_improvement',	'groups_diversity_improvement',	'groups_richness_improvement',
                                                        'species_diversity_improvement','species_richness_improvement',	'value_increase']])


        features_matrix = pd.DataFrame(features_matrix, index=index, columns=['species_diversity_improvement',
                                                                              'species_richness_improvement',
                                                                              'groups_diversity_improvement',
                                                                              'groups_richness_improvement',
                                                                              'carbon_storage_improvement',
                                                                              'value_increase'])
        features['score'] = features_matrix.apply(lambda row: w1 * row['species_diversity_improvement'] +
                                                              w2 * row['species_richness_improvement'] + w3 * row[
                                                                  'groups_diversity_improvement']
                                                              + w4 * row['groups_richness_improvement'] + w5 * row[
                                                                  'carbon_storage_improvement']
                                                              + w6 * row['value_increase'], axis=1)

        # Enrich dataframe with some other data 
        features['species_latin'] = features.apply(lambda row: self.get_species_row(row.name), axis=1)
        features['func_group'] = features.apply(lambda row: self.get_group_row(row.name), axis=1)
        features['family'] = features.apply(lambda row: self.get_family_row(row.name), axis=1)
        features['genus'] = features.apply(lambda row: self.get_genus_row(row.name), axis=1)
        features['condition'] = features.apply(lambda row: self.get_condition_row(row.name), axis=1)
        features['image_url'] = features.apply(lambda row: self.get_image_row(row.name), axis=1)
        features['hq_details_url'] = features.apply(lambda row: self.get_details_url_row(row.name), axis=1)

        # Generate the ranking, ordering by score
        result = []
        result = features.sort_values(by='score', ascending=False)
        result = result[result.score > 0]
        result = result.dropna()
        self.ranking  = result
        result = result[['score', 'species_latin', 'func_group', 'genus', 'family', 'species_diversity_improvement',
                         'species_richness_improvement', 'groups_diversity_improvement',
                         'groups_richness_improvement', 'carbon_storage_improvement', 'value_increase']]
        
        return result

    def agg_data(self, dataframe, data, criteria):
        df_from_dict = pd.DataFrame.from_dict(data, orient='index')
        group_data = dataframe.groupby(['uqam_code', criteria]).sum()  # sum function
        group_data = group_data.reset_index()
        group_data = group_data.set_index('uqam_code')
        merged_data = (pd.merge(df_from_dict, group_data, left_index=True, right_index=True))
        merged_data['uqam_code'] = merged_data.index
        merged_data = merged_data.drop(columns=['DBH', '_version_'])
        group_merged_data = merged_data.groupby([criteria]).sum()
        group_merged_data.transpose()
        # That's it !!!!!
        dict_y = group_merged_data[0].to_dict()
        dict_y = self.round_dict_values(dict_y, 3)
        return dict_y

    def build_table(self, trees_set_dataframe, df_content, col_name, carbon_storage=None, replacement_value=None, value_by_species=None ):
        
        start_time = time.monotonic()

        df = trees_set_dataframe
        carbon_storage_family = self.agg_data(df, carbon_storage, 'family')
        carbon_storage_genus = self.agg_data(df, carbon_storage, 'genus')
        carbon_storage_func_group = self.agg_data(df, carbon_storage, 'func_group')

        value_family = self.agg_data(df, value_by_species, 'family')
        value_genus = self.agg_data(df, value_by_species, 'genus')
        value_func_group = self.agg_data(df, value_by_species, 'func_group')
       
        species_list = []
        for key in df_content.to_dict().keys():
            species_row = {}
            species_row[col_name] = key
            species_row["Nb d'arbres"] = df_content.to_dict().get(key, "0")
            if col_name == 'Family':
                species_row["Carbon storage"] = carbon_storage_family.get(key, "0")
                species_row["Valeur"] = value_family.get(key, "0")
            elif col_name == 'Species Name':
                species_row["Carbon storage (Kg)"] = carbon_storage.get(key, "0")
                species_row["Valeur"] = value_by_species.get(key, "0")
            elif col_name == 'Species':
                P = df_content.reset_index()
                species_row["Percentage"] = round((100 * P.loc[P['specie_latin_cleaned'] == key, 'id']/ P['id'].sum()).values[0],2)

            elif col_name == 'Species_rep':
                P = df_content.reset_index()
                species_row["Replacement value (CAD)"] = replacement_value.get(key, "0")
                species_row["Percentage"] = round((100 * P.loc[P['specie_latin_cleaned'] == key, 'id']/ P['id'].sum()).values[0],2)      
            elif col_name == 'Species_pred':
                P = df_content.reset_index()
                species_row["Percentage"] = round((100 * P.loc[P['specie_latin_cleaned'] == key, 'tree_id']/ P['tree_id'].sum()).values[0],2)
            elif col_name == 'Genus':
                species_row["Carbon storage"] = carbon_storage_genus.get(key, "0")
                species_row["Valeur"] = value_genus.get(key, "0")
            elif col_name == 'Func Group pred':
                P = df_content.reset_index()
                species_row["Percentage"] = round((100 * P.loc[P['func_group'] == key, 'tree_id']/ P['tree_id'].sum()).values[0],2)
            elif col_name == 'Func Group':
                P = df_content.reset_index()
                species_row["Percentage"] = round((100 * P.loc[P['func_group'] == key, 'id']/ P['id'].sum()).values[0],2)
            else:
                species_row["Carbon storage"] = carbon_storage.get(key, "0")
                species_row["Valeur"] = value_by_species.get(key, "0")
            species_list.append(species_row)
        print('seconds: ', time.monotonic() - start_time)
        # print("species_list")
        # print(species_list)
        return species_list

    def get_old_trees_funcgroup(self, trees_set_dataframe, species_df):
        # (trees_set_dataframe.groupby('func_group')).count()['id'].to_dict()
        # print('get_old_trees_funcgroup')
        df_merged = pd.merge(trees_set_dataframe, species_df[['uqam_code', 'new_max_DBH']], right_on='code', left_on='uqam_code',
                             how='inner')
        new = df_merged["DBH"] >= df_merged["new_max_DBH"]
        
        df_merged["New"] = new
        old_trees = df_merged.loc[df_merged['New'] == True].reset_index(drop=True)
        nb_trees_bygroup = (df_merged.groupby('func_group')).count()['id'].to_dict()
        nb_old_bygroup = (old_trees.groupby('func_group')).count()['id'].to_dict()
        # nb_trees_bygroup_test = (df_merged.groupby('func_group')).count()['id']
        # nb_trees_bygroup_test['old'] = (old_trees.groupby('func_group')).count()['id']

        # print(nb_trees_bygroup_test)
        # print(nb_old_bygroup_test)
        return nb_old_bygroup
        # return trees_set_dataframe['New'].sum()

    def get_status(self, trees_set_dataframe, species_df):
        """Generates some analytics about the set of trees that represents a given area.
        
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.        
        Returns:
            A pandas DataFrame containing such analytics
        """

        df = trees_set_dataframe
        # Conversion d'une liste en serie panda
        curr_status = pd.Series()
        # curr_status['number_trees'] = len(df)
        # count number of rows in dataframe
        curr_status['number_trees'] = len(df.index)  # meilleur temps d'execution
        
        # Recuperation des groupes fonctionnels
        curr_status['func_groups_list'] = self.get_area_groups(df)
        curr_status['dbh_classes'] = self.get_dbh_classes(df)
        curr_status['count_by_species'] = (df.groupby('specie_latin_cleaned')).count()['id'].to_dict()
        curr_status['count_by_genus'] = (df.groupby('genus')).count()['id'].to_dict()
        curr_status['count_by_family'] = (df.groupby('family')).count()['id'].to_dict()
        curr_status['percentage_by_species'] = self.get_percentage_species(df)
        curr_status['percentage_by_genus'] = self.get_percentage_genus(df)
        curr_status['percentage_by_family'] = self.get_percentage_family(df)
        curr_status['count_by_func_group'] = (df.groupby('func_group')).count()['id'].to_dict()
        curr_status['old_trees_by_group'] = self.get_old_trees_funcgroup(trees_set_dataframe, species_df)
        curr_status['top_species'] = ((df.groupby('specie_latin_cleaned')).count()['id'].nlargest(10)).to_dict()
        curr_status['top_species_all'] = ((df.groupby('specie_latin')).count()['id'].nlargest(10)).to_dict()
        curr_status['carbon_storage_by_species'] = self.carbon_storage_by_species(df)
        curr_status['replacement_value_by_species'] = self.replacement_value_by_species(df)
        curr_status['value_by_species'] = self.value_by_species(df)
        curr_status['old_trees_table'] = self.get_old_trees(pd.merge(df, species_df[['uqam_code', 'new_max_DBH']], right_on='code', left_on='uqam_code',
                             how='inner'), convertToDict = True)
        #####
        carbon_storage = self.carbon_storage_by_species(df, all=True)
        replacement_value = self.replacement_value_by_species(df, all=True)
        value_by_species = self.value_by_species(df, all=True)
        #####
        # value_family = self.agg_data(df, value_by_species, 'family')
        # value_genus = self.agg_data(df, value_by_species, 'genus')
        # value_func_group = self.agg_data(df, value_by_species, 'func_group')
        #
        curr_status['species_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['id'], 'Species', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        curr_status['replacement_value_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['id'], 'Species_rep', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        curr_status['carbon_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['id'], 'Species Name', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        curr_status['func_group_table'] = self.build_table(df, (df.groupby('func_group')).count()['id'], 'Func Group', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        curr_status['genus_table'] = self.build_table(df, (df.groupby('genus')).count()['id'], 'Genus', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        curr_status['family_table'] = self.build_table(df, (df.groupby('family')).count()['id'], 'Family', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        # Count distinct elements of specie_latin in dataframe
        curr_status['different_species'] = df['specie_latin_cleaned'].nunique()
        # Uniques values of specie_latin are returned in order of appearance.
        curr_status['species_list'] = df['specie_latin_cleaned'].unique()
        # Count distinct elements of genus in dataframe
        curr_status['different_genus'] = df['genus'].nunique()
        # Uniques values of genus are returned in order of appearance.
        curr_status['genus_list'] = df['genus'].dropna().unique()
        curr_status['different_families'] = df['family'].nunique()
        curr_status['families'] = df['family'].dropna().unique()
        # Calcul de la diversite d'especes dans une aire donnee
        curr_status['species_diversity'] = self.area_effective_diversity_species(df)
        # Calcul de la diversite des groupes fonctionnels
        curr_status['group_diversity'] = self.area_effective_diversity_groups(df)
        # Calcul de l'ampleur de la diversite d'espece
        curr_status['species_richness'] = self.area_species_richness(df)
        curr_status['group_richness'] = self.area_groups_richness(df)
        curr_status['total_carbon_storage'] = self.total_carbon_storage(df)
        curr_status['total_replacement_value'] = self.total_replacement_value(df)
        curr_status['total_value'] = self.total_value(df)
        curr_status['func_groups_list'] = self.get_area_groups(df)

        # curr_status.to_csv("curr_status.csv")
        return curr_status

    def get_status_selected_trees(self, trees_set_dataframe):
        """Generates some analytics about the set of trees that represents a selected area.

        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        Returns:
            A pandas DataFrame containing such analytics
        """
        df = trees_set_dataframe
        # Conversion d'une liste en serie panda
        curr_status = pd.Series()

        # curr_status['number_trees'] = len(df)
        # count number of rows in dataframe
        curr_status['number_trees'] = len(df.index)  # meilleur temps d'execution
        # Recuperation des arbres ayant atteint le DBH max
        curr_status['nb_old_trees'] = len(self.get_old_trees(df))

        return curr_status


    def predict_status(self, trees_set_dataframe, list_new_species):
        """ Generates a prediction of how adding a new tree would impact the status of an area.
        
        Args: 
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area. 
            list_new_species : list of Latin name for the new species with the number of new trees associated
        Returns:
            A pandas Dataframe containing status before and after adding new trees to that area.
        """
        df = trees_set_dataframe
        curr_status = pd.Series()
        curr_status['number_trees'] = len(df.index)
        curr_status['species_diversity'] = self.area_effective_diversity_species(df)
        curr_status['group_diversity'] = self.area_effective_diversity_groups(df)
        curr_status['species_richness'] = self.area_species_richness(df)
        curr_status['group_richness'] = self.area_groups_richness(df)
        curr_status['total_carbon_storage'] = self.total_carbon_storage(df)
        curr_status['total_value'] = self.total_value(df)


        # print("df before adding tree")
        # print(df.info(verbose=True))
        # print(list(df.columns) )
        if len(list_new_species)>0:
            for new_specie in list_new_species:
                new_species_name = new_specie["name"]
                new_species_number = new_specie["nb_suggested"]
                tree = self.candidates_df.loc[new_species_name]
                
                # Add new tree
                for i in range(int(new_species_number)):
                    tree['tree_id'] = len(df) + 1
                    tree['specie_latin_cleaned'] = tree['specie_mtl']
                    # print(type(tree))
                    #tree.rename({'specie_mtl': 'species_latin'})
                    # print(tree)
                    df = df.append(tree, ignore_index=True)
        
        prediction = pd.Series()
        prediction['number_trees'] = len(df.index)  # meilleur temps d'execution
        prediction['species_diversity'] = self.area_effective_diversity_species(df)
        prediction['group_diversity'] = self.area_effective_diversity_groups(df)
        prediction['species_richness'] = self.area_species_richness(df)
        prediction['group_richness'] = self.area_groups_richness(df)
        prediction['total_carbon_storage'] = self.total_carbon_storage(df)
        prediction['total_value'] = self.total_value(df)
        # print(prediction)

        info_prediction = pd.Series()
        # Recuperation des groupes fonctionnels
        info_prediction['number_trees'] = len(df.index)  # meilleur temps d'execution
        info_prediction['func_groups_list'] = self.get_area_groups(df)
        info_prediction['count_by_species'] = (df.groupby('specie_latin_cleaned')).count()['tree_id'].to_dict()
        info_prediction['count_by_genus'] = (df.groupby('genus')).count()['tree_id'].to_dict()
        info_prediction['count_by_family'] = (df.groupby('family')).count()['tree_id'].to_dict()
        info_prediction['percentage_by_species'] = self.get_percentage_species(df)
        info_prediction['percentage_by_genus'] = self.get_percentage_genus(df)
        info_prediction['percentage_by_family'] = self.get_percentage_family(df)
        info_prediction['count_by_func_group'] = (df.groupby('func_group')).count()['tree_id'].to_dict()
        info_prediction['top_species'] = ((df.groupby('specie_latin_cleaned')).count()['tree_id'].nlargest(10)).to_dict()
        info_prediction['carbon_storage_by_species'] = self.carbon_storage_by_species(df)
        info_prediction['value_by_species'] = self.value_by_species(df)
        #####
        carbon_storage = self.carbon_storage_by_species(df, all=True)
        replacement_value = None
        # replacement_value = self.replacement_value_by_species(df, all=True)
        value_by_species = self.value_by_species(df, all=True)
        #####
        # value_family = self.agg_data(df, value_by_species, 'family')
        # value_genus = self.agg_data(df, value_by_species, 'genus')
        # value_func_group = self.agg_data(df, value_by_species, 'func_group')
        #
        # df.to_csv("prediction.csv")
        info_prediction['species_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['tree_id'], 'Species_pred', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        # info_prediction['replacement_value_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['id'], 'Species_rep', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        info_prediction['carbon_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['id'], 'Species Name', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        # info_prediction['carbon_table'] = self.build_table(df, (df.groupby('uqam_code')).count()['tree_id'], 'Carbon', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        info_prediction['func_group_table'] = self.build_table(df, (df.groupby('func_group')).count()['tree_id'], 'Func Group pred', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        info_prediction['genus_table'] = self.build_table(df, (df.groupby('genus')).count()['tree_id'], 'Genus', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
        info_prediction['family_table'] = self.build_table(df, (df.groupby('family')).count()['tree_id'], 'Family', carbon_storage=carbon_storage, replacement_value=replacement_value, value_by_species=value_by_species)
       


        # info_prediction['species_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['tree_id'], 'Species_pred')
        # info_prediction['func_group_table'] = self.build_table(df, (df.groupby('func_group')).count()['tree_id'], 'Func Group pred')
        # info_prediction['genus_table'] = self.build_table(df, (df.groupby('genus')).count()['tree_id'], 'Genus')
        # info_prediction['family_table'] = self.build_table(df, (df.groupby('family')).count()['tree_id'], 'Family')
        # info_prediction['carbon_table'] = self.build_table(df, (df.groupby('specie_latin_cleaned')).count()['id'], 'Carbon')
        # Count distinct elements of specie_latin in dataframe
        info_prediction['different_species'] = df['specie_latin_cleaned'].nunique()
        # Uniques values of specie_latin are returned in order of appearance.
        info_prediction['species_list'] = df['specie_latin_cleaned'].unique()
        # Count distinct elements of genus in dataframe
        info_prediction['different_genus'] = df['genus'].nunique()
        # Uniques values of genus are returned in order of appearance.
        info_prediction['genus_list'] = df['genus'].dropna().unique()
        info_prediction['different_families'] = df['family'].nunique()
        info_prediction['families'] = df['family'].dropna().unique()
        # Calcul de la diversite d'especes dans une aire donnee
        info_prediction['species_diversity'] = self.area_effective_diversity_species(df)
        # Calcul de la diversite des groupes fonctionnels
        info_prediction['group_diversity'] = self.area_effective_diversity_groups(df)
        # # Calcul de l'ampleur de la diversite d'espece
        info_prediction['species_richness'] = self.area_species_richness(df)
        info_prediction['group_richness'] = self.area_groups_richness(df)
        info_prediction['total_carbon_storage'] = self.total_carbon_storage(df)
        info_prediction['total_value'] = self.total_value(df)
        info_prediction['func_groups_list'] = self.get_area_groups(df)
        info_status = pd.DataFrame()
        info_status['info_prediction'] = info_prediction
        info_status.dropna()
        status = pd.DataFrame()
        status['curr_status'] = curr_status
        status['prediction'] = prediction
        status['improvement'] = status['prediction'] - status['curr_status']
        status['improvement_percentage'] = (status['improvement'] / status['curr_status'])*100
        # print("Status")
        # status = status.round(2)
        # info_status = info_status.round(2)
        # # status = status[~status.isin([np.nan, np.inf, -np.inf]).any(1)]
        # # info_status = info_status[~info_status.isin([np.nan, np.inf, -np.inf]).any(1)]
        # status.to_csv('status_test.csv')
        # info_status.to_csv('info_status_test.csv')
        # print(status)
        # print('Check nans info_status')
        # print(info_status.isnull().values.any())
        # print('Check nans status')
        # print(status.isnull().values.any())
        # print('status describe')
        # print(status.describe())
        # print('info_status describe')
        # print(info_status.describe())

        return status, info_status

    def features_extractor(self, trees_set_dataframe, trees_location):
        """Calculates the improvement of each tree related to different aspects.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
            trees_location : street, parks, all
        Returns:
           A pandas Dataframe that contains a matrix of features.
        """
        # print(trees_set_dataframe.columns)
        species_diversity = self.area_effective_diversity_species(trees_set_dataframe)
        species_richness = self.area_species_richness(trees_set_dataframe)
        groups_diversity = self.area_effective_diversity_groups(trees_set_dataframe)
        groups_richness = self.area_groups_richness(trees_set_dataframe)
        c_storage = self.total_carbon_storage(trees_set_dataframe)
        area_value = self.total_value(trees_set_dataframe)
        area_groups = self.get_area_groups(trees_set_dataframe)



        features_matrix = []

        for index, tree in self.candidates_df.iterrows():
            tree['tree_id'] = len(trees_set_dataframe) + 1
            df = trees_set_dataframe.append(tree)

            # Ignore species with missing data
            if pd.isnull(tree['uqam_code']) or pd.isnull(tree['genus']) or pd.isnull(tree['family']):
                continue
            # Ignore species that doesn't respect the 10-20-30 rule
            if tree['uqam_code'].lower() in self.violation_10_rule(df)\
                    or tree['genus'].lower() in self.violation_20_rule(df) \
                    or tree['func_group'] in self.violation_30_rule(df):
                continue
            if trees_location == "street" and tree['Rue'].lower() != "oui":
                continue
            elif trees_location == "parks" and tree["Parc"].lower() != "oui":
                continue

            species_diversity_improv = self.improvement_effective_diversity_species(tree['uqam_code'],
                                                                                    species_diversity, species_richness,
                                                                                    df)
            species_richness_improv = self.improvement_richness_species(species_richness, df)
            groups_diversity_improv = self.improvement_effective_diversity_groups(tree['func_group'], groups_diversity,
                                                                                  groups_richness, df)
            groups_richness_improv = self.improvement_richness_groups(groups_richness, df)
            # DBH = self.species_df['new_max_DBH'].mean()
            DBH = 15
            # if tree['code'] in self.species_df.index.values:
            #     if not math.isnan(self.species_df['new_max_DBH'][tree['code']]):
            #         DBH = self.species_df['new_max_DBH'][tree['code']]
            c_storage_improv = self.improvement_carbon_storage(c_storage, tree['uqam_code'], DBH)
            value_increase = self.improvement_value(area_value, tree['uqam_code'], DBH)

            candidate = {}
            candidate['species_latin'] = index
            candidate['species_diversity_improvement'] = 100 * species_diversity_improv
            candidate['species_richness_improvement'] = 100 * species_richness_improv
            candidate['groups_diversity_improvement'] = 100 * groups_diversity_improv
            candidate['groups_richness_improvement'] = 100 * groups_richness_improv
            candidate['carbon_storage_improvement'] = 100 * c_storage_improv
            candidate['value_increase'] = 100 * value_increase
            features_matrix.append(candidate)

        features = pd.DataFrame(features_matrix).set_index('species_latin')
        return features

    def define_area(self, trees_set_dataframe):
        """Define a new set of trees.
      
        Args:
            trees_set_dataframe: A Pandas DataFrame expected to contain data
                about trees in a specific area.
        """
        self.trees_set_dataframe = trees_set_dataframe
