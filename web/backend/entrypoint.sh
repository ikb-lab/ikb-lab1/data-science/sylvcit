#!/bin/sh


while ! nc -z solr 8983; do
    echo 'Waiting for solr'
    sleep 5
done

echo "Solr started"

#python manage.py flush --no-input
#python manage.py migrate
python manage.py runserver 0.0.0.0:8000

exec "$@"

