import React, {Component} from 'react';
import {connect} from 'react-redux';
import {selection} from '../actions/actions';


/*const districts = [
	{id:0 , district: 'Ahuntsic - Cartierville'},
	{id:1 , district: 'Saint-Léonard'},
	{id:2 , district: 'Ville-Marie'},
	{id:3 , district: 'Côte-des-Neiges - Notre-Dame-de-Grâce'},
	{id:4 , district: 'Verdun'},
	{id:5 , district: 'Rosemont - La Petite-Patrie'},
];
*/

class SelectDistrict extends Component{
	
	handleClick = e => {
		//const district = districts[parseInt(e.target.value)].district
		this.props.dispatch(selection(e.target.value));
	}

	render(){
		//const {selected} = this.props;
		
		return(
			<div>
				<div>Selected: {this.props.selected}</div>
				<div className="ui radio checkbox">
					<input type="radio" name="district" value="0" onClick={this.handleClick}/>
					<label> Ahuntsic - Cartierville</label> 
  			   	</div>
  			   	<br/>
  			   	<div className="ui radio checkbox">
					<input type="radio" name="district" value="1" onClick={this.handleClick}/>
	  			  	<label> Saint-Léonard </label> 
	  			</div>
	  			<br/>

  			   	<div className="ui radio checkbox">
					<input type="radio" name="district" value="2" onClick={this.handleClick}/>	
					<label> Ville-Marie </label> 
				</div>
				<br/>	


  			   	<div className="ui radio checkbox">
					<input type="radio" name="district" value="3" onClick={this.handleClick}/>		
					<label> Côte-des-Neiges - Notre-Dame-de-Grâce </label> 
				</div>
				<br/>

				<div className="ui radio checkbox">
					<input type="radio" name="district" value="4" onClick={this.handleClick}/>
					<label> Verdun </label> 
				</div>
				<br/>

				<div className="ui radio checkbox">
					<input type="radio" name="district" value="5" onClick={this.handleClick}/>
					<label> Rosemont - La Petite-Patrie </label> 
				</div>
				<br/>
			</div>
		);	
	}
}

const mapStateToProps = state => ({
	selected: state.selected
});

export default connect(mapStateToProps)(SelectDistrict);
