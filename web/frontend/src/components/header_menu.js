import React from 'react'
import {
  Container,
  Divider,
  Dropdown,
  Grid,
  Header,
  Image,
  List,
  Menu,
  Segment,
} from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import Export from './SylvPDFTemplate'
const HeaderMenu = () => (
  <div>
    <Menu inverted>
      <Container>
        <Menu.Item  header>
          <Image size='mini' src={require('../images/logo.png')} />
          <Link to="/">SylvCiT</Link>
        </Menu.Item>
        <Menu.Item  header>
          {/* <Link to="/">UQAM</Link> */}
        </Menu.Item>
        <Menu.Item header>
      <Link to="/about">About</Link>
        </Menu.Item>
      </Container>
    </Menu>
  </div>
)

export default HeaderMenu