import React from 'react';
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'

export default function SylvLoader({ active, children, message }) {
  return (
    <LoadingOverlay
      active={active}
      spinner={<BounceLoader />}
      text={message}
      styles={{
        spinner: (base) => ({
          ...base,
          width: '100px',
          '& svg circle': {
          stroke: '#2BAD60'
          }
        })
        }}
    >
      {children}
    </LoadingOverlay>
  )
}