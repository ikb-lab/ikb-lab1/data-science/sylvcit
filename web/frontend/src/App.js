import React, { useState } from 'react';
import { Route } from "react-router-dom";
import Home from "./pages/Home"
import Habitat from "./pages/Habitat"
// import TreeMap from "./components/map"
import Status from "./pages/Status"
import NewStatus from "./pages/NewStatus"
import Settings from "./pages/Settings"
import SelectGroup from "./pages/SelectGroup"
import SelectSpecies from "./pages/SelectSpecies"

import {
  Container,
  Select,
  Image,
  Menu,
} from 'semantic-ui-react'
import { Link } from 'react-router-dom';

import { IntlProvider, FormattedMessage } from "react-intl";

import en from "./locale/en.json";
import fr from "./locale/fr.json";

export default function App() {
  const [locale, setLocale] = useState("fr");

  const messages = { en, fr };

  const handleChange = (event, data)  => {
    console.log(event)
    console.log(data.value);
    setLocale(data.value);
  };

  const options = [
    { key: 'en', text: 'en', value: 'en' },
    { key: 'fr', text: 'fr', value: 'fr' },
  ]

  return (
    <IntlProvider locale={locale} messages={messages[locale]}>
      <Menu inverted className="noMargin">
        <Container>
          <Menu.Item header>
            {/* <Image size='mini' src={require('./images/logo.png')} /> */}
            <Image size='mini' src="/images/logo.png" />
            <Link to="/">SylvCiT</Link>
          </Menu.Item>

          {/* <Menu.Item header>
            <Link to="/about">À propos</Link>
          </Menu.Item> */}

          <Menu.Menu position='right'>
          <Menu.Item>
          <Link to={{ pathname: "https://cloud.labikb.ca/index.php/s/racFRYMZqiSSboP/download" }} target="_blank"><FormattedMessage id="home.user_guide" /></Link>
          </Menu.Item>
          <Menu.Item>
            {/* <Image size='small' src={require('./images/uqam_logo.png')} /> */}
            <Image size='small' src="/images/uqam_logo.png" />
          </Menu.Item>
            <Menu.Item
              name='logout'
            >
              <Select compact options={options} defaultValue='fr' onChange={handleChange}/>
            </Menu.Item>
          </Menu.Menu>
        </Container>
      </Menu>

      <div className="ui container">
        <Route path="/" exact component={Home} />
        <Route path="/habitat" exact component={Habitat} />
        <Route path="/status" exact component={Status} />
        <Route path="/newstatus" exact component={NewStatus} />
        <Route path="/settings" exact component={Settings} />
        <Route path="/selectgroup" exact component={SelectGroup} />
        <Route path="/selectspecies" exact component={SelectSpecies} />
      </div>
    </IntlProvider>
  );
}
