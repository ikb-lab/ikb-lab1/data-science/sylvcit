import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import App from './App';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import "semantic-ui-css/semantic.min.css"
import registerServiceWorker from './services/registerServiceWorker';
import reducer from './reducers/reducers';
import './styles/index.css'



const store = createStore(reducer);

ReactDOM.render( 
	<BrowserRouter>
		<Provider store={store}>
		
			<App />
		</Provider>
	</BrowserRouter>,
	document.getElementById('root')
);
registerServiceWorker();
