
# coding: utf-8

# <h1>Gower distance calculation for Python V6.4</h1>
# <h3>Version submited to scikit learn project approval</h3>
# https://github.com/scikit-learn/scikit-learn/pull/9555
# 
# <p>It is not all the time that the data under study is an even matrix of numerical values. Sometimes, you need to dig into data with mixed types of variables (e.g., categorical, boolean, numerical).
# </p>
# <p>This notebook provides the Gower function that calculates the Gower mixed similarity.
# </p>
# <p>For more details about the Gower distance, please visit: <a href="http://members.cbio.mines-paristech.fr/~jvert/svn/bibli/local/Gower1971general.pdf">Gower, J.C., 1971, A General Coefficient of Similarity and Some of Its Properties</a>.</p>
# 

# <h2>1. Generate some data with mixed types</h2>

# In[1]:


import numpy as np
import pandas as pd
from scipy.spatial import distance #
from sklearn.utils import validation
from sklearn.metrics import pairwise
from scipy.sparse import issparse


# X=pd.DataFrame({'age':[21,21,19, 30,21,21,19,30,None],
# 'gender':['M','M','N','M','F','F','F','F',None],
# 'civil_status':['MARRIED','SINGLE','SINGLE','SINGLE','MARRIED','SINGLE','WIDOW','DIVORCED',None],
# 'salary':[3000.0,1200.0 ,32000.0,1800.0 ,2900.0 ,1100.0 ,10000.0,1500.0,None],
# 'has_children':[True,False,True,True,True,False,False,True,None],
# 'available_credit':[2200,100,22000,1100,2000,100,6000,2200,None]})


# print(X)


# #2. Check pairwise utility functios (version not released yet to scikit-learn)

# In[2]:

def check_pairwise_arrays(X, Y, precomputed=False, dtype=None):
    X, Y, dtype_float = pairwise._return_float_dtype(X, Y)

    warn_on_dtype = dtype is not None
    estimator = 'check_pairwise_arrays'
    if dtype is None:
        dtype = dtype_float

    if Y is X or Y is None:
        X = Y = validation.check_array(X, accept_sparse='csr', dtype=dtype,
                            warn_on_dtype=warn_on_dtype, estimator=estimator)
    else:
        X = validation.check_array(X, accept_sparse='csr', dtype=dtype,
                        warn_on_dtype=warn_on_dtype, estimator=estimator)
        Y = validation.check_array(Y, accept_sparse='csr', dtype=dtype,
                        warn_on_dtype=warn_on_dtype, estimator=estimator)

    if precomputed:
        if X.shape[1] != Y.shape[0]:
            raise ValueError("Precomputed metric requires shape "
                             "(n_queries, n_indexed). Got (%d, %d) "
                             "for %d indexed." %
                             (X.shape[0], X.shape[1], Y.shape[0]))
    elif X.shape[1] != Y.shape[1]:
        raise ValueError("Incompatible dimension for X and Y matrices: "
                         "X.shape[1] == %d while Y.shape[1] == %d" % (
                             X.shape[1], Y.shape[1]))

    return X, Y


# # 3. The Gower Function

# In[10]:



# Vectorized Version
def gower_distances(X, Y=None, feature_weight=None, categorical_features=None):
    """Computes the gower distances between X and Y

    Gower is a similarity measure for categorical, boolean and numerical mixed
    data.

    
    Parameters
    ----------
    X : array-like, or pandas.DataFrame, shape (n_samples, n_features)

    Y : array-like, or pandas.DataFrame, shape (n_samples, n_features)

    feature_weight :  array-like, shape (n_features)
        According the Gower formula, feature_weight is an attribute weight.

    categorical_features: array-like, shape (n_features)
        Indicates with True/False whether a column is a categorical attribute.
        This is useful when categorical atributes are represented as integer
        values. Categorical ordinal attributes are treated as numeric, and must
        be marked as false.
        
        Alternatively, the categorical_features array can be represented only
        with the numerical indexes of the categorical attribtes.

    Returns
    -------
    similarities : ndarray, shape (n_samples, n_samples)

    Notes
    ------
    The non-numeric features, and numeric feature ranges are determined from X and not Y.
    No support for sparse matrices.

    """
    
    if issparse(X) or issparse(Y):
        raise TypeError("Sparse matrices are not supported for gower distance")
        
    y_none = Y is None
    
    
    # It is necessary to convert to ndarray in advance to define the dtype
    if not isinstance(X, np.ndarray):
        X = np.asarray(X)

    array_type = np.object
    # this is necessary as strangelly the validator is rejecting numeric
    # arrays with NaN
    if  np.issubdtype(X.dtype, np.number) and (np.isfinite(X.sum()) or np.isfinite(X).all()):
        array_type = type(np.zeros(1,X.dtype).flat[0])
    
    X, Y = check_pairwise_arrays(X, Y, precomputed=False, dtype=array_type)
    
    n_rows, n_cols = X.shape
    
    if categorical_features is None:
        categorical_features = np.zeros(n_cols, dtype=bool)
        for col in range(n_cols):
            # In numerical columns, None is converted to NaN,
            # and the type of NaN is recognized as a number subtype
            if not np.issubdtype(type(X[0, col]), np.number):
                categorical_features[col]=True
    else:          
        categorical_features = np.array(categorical_features)
    
    
    #if categorical_features.dtype == np.int32:
    if np.issubdtype(categorical_features.dtype, np.int):
        new_categorical_features = np.zeros(n_cols, dtype=bool)
        new_categorical_features[categorical_features] = True
        categorical_features = new_categorical_features
    
    print(categorical_features)
  
    # Categorical columns
    X_cat =  X[:,categorical_features]
    
    # Numerical columns
    X_num = X[:,np.logical_not(categorical_features)]
    ranges_of_numeric = None
    max_of_numeric = None
    
        
    # Calculates the normalized ranges and max values of numeric values
    _ ,num_cols=X_num.shape
    ranges_of_numeric = np.zeros(num_cols)
    max_of_numeric = np.zeros(num_cols)
    for col in range(num_cols):
        col_array = X_num[:, col].astype(np.float32) 
        max = np.nanmax(col_array)
        min = np.nanmin(col_array)
     
        if np.isnan(max):
            max = 0.0
        if np.isnan(min):
            min = 0.0
        max_of_numeric[col] = max
        ranges_of_numeric[col] = (1 - min / max) if (max != 0) else 0.0


    # This is to normalize the numeric values between 0 and 1.
    X_num = np.divide(X_num ,max_of_numeric,out=np.zeros_like(X_num), where=max_of_numeric!=0)

    
    if feature_weight is None:
        feature_weight = np.ones(n_cols)
        
    feature_weight_cat=feature_weight[categorical_features]
    feature_weight_num=feature_weight[np.logical_not(categorical_features)]
    
    
    y_n_rows, _ = Y.shape
    
    dm = np.zeros((n_rows, y_n_rows), dtype=np.float32)
        
    feature_weight_sum = feature_weight.sum()

    Y_cat=None
    Y_num=None
    
    if not y_none:
        Y_cat = Y[:,categorical_features]
        Y_num = Y[:,np.logical_not(categorical_features)]
        # This is to normalize the numeric values between 0 and 1.
        Y_num = np.divide(Y_num ,max_of_numeric,out=np.zeros_like(Y_num), where=max_of_numeric!=0)
    else:
        Y_cat=X_cat
        Y_num = X_num
        
    for i in range(n_rows):
        j_start= i
        
        # for non square results
        if n_rows != y_n_rows:
            j_start = 0

      
        Y_cat[j_start:n_rows,:]
        Y_num[j_start:n_rows,:]
        result= _gower_distance_row(X_cat[i,:], X_num[i,:],Y_cat[j_start:n_rows,:],
                                    Y_num[j_start:n_rows,:],feature_weight_cat,feature_weight_num,
                                    feature_weight_sum,categorical_features,ranges_of_numeric,
                                    max_of_numeric) 
        dm[i,j_start:]=result
        dm[i:,j_start]=result
        

    return dm


def _gower_distance_row(xi_cat,xi_num,xj_cat,xj_num,feature_weight_cat,feature_weight_num,
                        feature_weight_sum,categorical_features,ranges_of_numeric,max_of_numeric ):
    # categorical columns
    sij_cat = np.where(xi_cat == xj_cat,np.zeros_like(xi_cat),np.ones_like(xi_cat))
    sum_cat = np.multiply(feature_weight_cat,sij_cat).sum(axis=1) 

    # numerical columns
    abs_delta=np.absolute( xi_num-xj_num)
    sij_num=np.divide(abs_delta, ranges_of_numeric, out=np.zeros_like(abs_delta), where=ranges_of_numeric!=0)

    sum_num = np.multiply(feature_weight_num,sij_num).sum(axis=1)
    sums= np.add(sum_cat,sum_num)
    sum_sij = np.divide(sums,feature_weight_sum)
    return sum_sij


# # 4. Get the Gower distance matrix

# In[11]:


# D = gower_distances(X)

# print(D)


# <h1>5. The equivalent code in R</h1>
# Using the daisy method from {cluster} package

# <p>
# <code>
# library(cluster)
# 
# age=c(21,21,19,30,21,21,19,30,NA)
# gender=c('M','M','N','M','F','F','F','F',NA)
# civil_status=c('MARRIED','SINGLE','SINGLE','SINGLE','MARRIED','SINGLE','WIDOW','DIVORCED',NA)
# salary=c(3000.0,1200.0 ,32000.0,1800.0 ,2900.0 ,1100.0 ,10000.0,1500.0,NA)
# children=c(TRUE,FALSE,TRUE,TRUE,TRUE,FALSE,FALSE,TRUE,NA)
# available_credit=c(2200,100,22000,1100,2000,100,6000,2200,NA)
# X=data.frame(age,gender,civil_status,salary,children,available_credit)
# 
# D=daisy(X,metric="gower")
# 
# print(D)
# 
# Dissimilarities :
#           1         2         3         4         5         6         7         8
# 2 0.3590238                                                                      
# 3 0.6707398 0.6964303                                                            
# 4 0.3178742 0.3138769 0.6552807                                                  
# 5 0.1687281 0.5236290 0.6728013 0.4824794                                        
# 6 0.5262298 0.2006472 0.6969697 0.4810829 0.3575017                              
# 7 0.5969786 0.5472028 0.7404280 0.7481861 0.4323733 0.3478501                    
# 8 0.4777876 0.6539635 0.8151941 0.3433228 0.3121036 0.4878362 0.5747661          
# 9        NA        NA        NA        NA        NA        NA        NA        NA
# 
# </code>

# # 5. Some unit tests in sklearn

# In[12]:


# from sklearn.utils.testing import assert_array_almost_equal

# X1 = np.array([['M', False, 222.22, 1],
#                   ['F', True, 333.22, 2],
#                   ['M', True, 1934.0, 4],
#                   [None, None, np.nan, np.nan]], dtype=object)

# D = gower_distances(X1)

# print(D)

# assert_array_almost_equal(D, np.array([[0.0, 0.599545, 0.75, np.nan],
#                                         [0.599545, 0.0, 0.650455, np.nan],
#                                         [0.75, 0.650455, 0.0, np.nan],
#                                         [np.nan, np.nan, np.nan, np.nan]]))


# # In[13]:


# X2 = np.array([['M', False, 222.22, 1],
#                   ['F', True, 333.22, 2],
#                   ['M', True, 1934.0, 4],
#                   [None, None, np.nan, np.nan]], dtype=object)

# D_expected = np.array([[0.0, 0.599545, 0.75, np.nan],
#                        [0.599545, 0.0, 0.650455, np.nan],
#                        [0.75, 0.650455, 0.0, np.nan],
#                        [np.nan, np.nan, np.nan, np.nan]])

# D = gower_distances(X2, categorical_features=[0, 1])

# print(D)


# # In[14]:


# from sklearn.utils.testing import assert_array_almost_equal

# X3 = np.array([['M', False],
#                   ['F', True],
#                   ['M', True],
#                   [None, None]], dtype=object)

# D = gower_distances(X3)

# print(D)

# assert_array_almost_equal(D, np.array([[0.0, 1, 0.5,1.0],
#                                         [1, 0.0, 0.5, 1.0],
#                                         [0.5, 0.5, 0.0, 1.0],
#                                         [1.0, 1.0, 1.0,0]]))


# # In[8]:


# from sklearn.utils.testing import assert_array_almost_equal


# X4 = np.array([[222.22, 1],
#                   [333.22, 2],
#                   [1934.0, 4],
#                   [1,1]],dtype=np.float32)

# D = gower_distances(X4)

# print(D)

# assert_array_almost_equal(D, np.array([[0., 0.19537851, 0.94277805 ,0.05722193],
#                                        [0.19537851, 0., 0.74739957, 0.25260046],
#                                        [0.94277805, 0.74739957, 0., 1.],
#                                        [0.05722193, 0.25260046, 1., 0]]))


# # # 6. Not Squared Matrix Test

# # In[9]:


# X5 = np.array([['Syria', 1200, 0,411114.44,True],
#                   ['Ireland', 300, 0, 199393333.22, False],
#                   ['United Kingdom', 100, 0, 32323222.121, False]], dtype=object)
               
# Y2 = np.array([['United Kingdom', 200, 0, 99923921.47, True]], dtype=object)


# D = gower_distances(X5,Y2)

# print("Result:",D)
# print()
# print("Expected:",[[ 0.48183999],[ 0.51816001],[ 0.28612829]])

               
               

