import math


class MTMConverter:
    def __init__(self):
        self.zone_to_refmeridian = [
            -0,
            -53,
            -56,
            -58.5,
            -61.5,
            -64.5,
            -67.5,
            -70.5,
            -73.5,
            -76.5,
            -79.5,
            -82.5,
            -81,
            -84,
            -87,
            -90,
            -93,
            -96,
            -99,
            -102,
            -105,
            -108,
            -111,
            -114,
            -117,
            -120,
            -123,
            -126,
            -129,
            -132,
            -135,
            -138,
            -141,
        ]
        self.SCALE = 0.9999
        self.EASTING_ORIGIN = 304800
        self.ECCENTRICITY = 0.00669438
        self.AXIS = 6378137
        self.HEMISPHERE = 1
        self.RAD_TO_DEG = 180 / math.pi

        self.E1 = (1 - math.sqrt(1 - self.ECCENTRICITY)) / (
            1 + math.sqrt(1 - self.ECCENTRICITY)
        )
        self.ECC_PRIME_SQUARE = self.ECCENTRICITY / (1 - self.ECCENTRICITY)

    def to_latlon(self, easting, northing, zone_number):
        x = easting - self.EASTING_ORIGIN
        y = northing
        long_origin = self.zone_number_to_central_longitude(zone_number)

        m = y / self.SCALE
        mu = m / (
            self.AXIS
            * (
                1
                - self.ECCENTRICITY / 4
                - 3 * self.ECCENTRICITY**2 / 64
                - 5 * self.ECCENTRICITY**3 / 256
            )
        )

        phi1_rad = (
            mu
            + (3 * self.E1 / 2 - 27 * self.E1**3 / 32) * math.sin(2 * mu)
            + (21 * self.E1**2 / 16 - 55 * self.E1**4 / 32) * math.sin(4 * mu)
            + (151 * self.E1**3 / 96) * math.sin(6 * mu)
        )
        n1 = self.AXIS / math.sqrt(
            1 - self.ECCENTRICITY * math.sin(phi1_rad) * math.sin(phi1_rad)
        )
        t1 = math.tan(phi1_rad) ** 2
        c1 = self.ECC_PRIME_SQUARE * math.cos(phi1_rad) ** 2
        r1 = (
            self.AXIS
            * (1 - self.ECCENTRICITY)
            / math.pow(1 - self.ECCENTRICITY * math.sin(phi1_rad) ** 2, 1.5)
        )
        d = x / (n1 * self.SCALE)

        lat = phi1_rad - (n1 * math.tan(phi1_rad) / r1) * (
            d**2 / 2
            - (5 + 3 * t1 + 10 * c1 - 4 * c1**2 - 9 * self.ECC_PRIME_SQUARE)
            * d**4
            / 24
            + (
                61
                + 90
                + t1
                + 298 * c1
                + 45 * t1**2
                - 252 * self.ECC_PRIME_SQUARE
                - 3 * c1**2
            )
            * d**6
            / 720
        )
        long = (
            d
            - (1 + 2 * t1 + c1) * d**3 / 6
            + (
                5
                - 2 * c1
                + 28 * t1
                - 3 * c1**2
                + 8 * self.ECC_PRIME_SQUARE
                + 24 * t1**2
            )
            * d**5
            / 120
        ) / math.cos(phi1_rad)

        lat = lat * self.RAD_TO_DEG
        long = long_origin + long * self.RAD_TO_DEG

        return lat, long

    def zone_number_to_central_longitude(self, zone_number):
        return self.zone_to_refmeridian[zone_number]


if __name__ == "__main__":
    converter = MTMConverter()
    print(converter.to_latlon(306164.923, 5037583.784, 8))
