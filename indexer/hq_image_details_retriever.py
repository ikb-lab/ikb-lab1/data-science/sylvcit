import json
import os, os.path
import pysolr
import csv
import re
import multiprocessing
import requests
from joblib import Parallel, delayed
from bs4 import BeautifulSoup
import datetime

documents = []
num_cores = multiprocessing.cpu_count()
hq_base_url = 'https://arbres.hydroquebec.com/'
class HQ_retriever:
    def __init__(self, file_path) :
        self.trees = self.read_species(file_path)
        print(len(self.trees))
        self.trees_hq_details = processed_list =Parallel(n_jobs=num_cores)(delayed(self.get_hq_details2)(species) for species in self.trees)
        self.time_now  = datetime.datetime.now().strftime('%d_%m_%Y_%H_%M_%S') 
        # self.index_documents(self.trees)
        self.export_to_csv(self.trees_hq_details)

    def read_species(self, file_path):
        list_trees = []
        filepath = os.path.abspath(file_path) ## ~ os.getcwd()
        with open(filepath, 'r', encoding='utf8') as csv_file:
            reader = csv.DictReader(csv_file)
            tree_id = 0
            for row in reader:
                tree = {}
                tree = row
                list_trees.append(tree)
            return list_trees

    def export_to_csv(self, documents):
        keys = documents[0].keys()
        with open('./outputs/candidates_'+self.time_now+'.csv', 'w',encoding='utf8', newline='')  as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(documents)

    def get_hq_details2(self, species):
        species_name = species["Latin"]
        try:
            url = "https://arbres.hydroquebec.com/resultat-arbres-arbustes?rechercheParNom="+species_name+"&type.id=37&plantationDistanceMinimum.id=9&zoneRusticite.id=42&forme.id=77&expositionLumiere.id=87&hauteur.id=57&solHumidite.id=92&largeur.id=67"
            respHQ = requests.get(url)
            # parse html
            page = BeautifulSoup(respHQ.content, features="html.parser")
            resultList = page.find("div", {"class": "l-resultat-item"})
            
            if resultList == -1:
                return 'no_data','no_data'
            else:
                figure = resultList.find("figure")
                if figure is not None :
                    tree_image = figure.find("img").get('src',None)
                    tree_details = figure.find("a").get('href',None)
                    species['image_url'], species['hq_details_url'] = hq_base_url+tree_image, hq_base_url+tree_details
                    # return hq_base_url+tree_image,hq_base_url+tree_details
                else:
                    species['image_url'], species['hq_details_url'] = 'no_data','no_data'
        except Exception as e:
            # print("exception")
            print(e)
            species['image_url'], species['hq_details_url'] = 'no_data','no_data'
        return species

# parserIndexer =  ParserIndexer('../data/trees_data/arbres-publics.csv')
hq_retriever =  HQ_retriever('../data/candidates_new_AS.csv')