import os, os.path
import pysolr
import csv
import re
import datetime
import pandas
import glob
import subprocess

from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from dateutil import parser

from conversion import MTMConverter

documents = []


class ParserIndexer:
    def __init__(self, file_path):
        self.mtm_converter = MTMConverter()
        self.unique_species = set()
        self.uqam_codes = self.read_uqam_code('../data/uqam_codes_new.csv')
        self.codes_data = pandas.read_csv('../data/uqam_codes_new.csv', sep=';')
        print("Reading and formatting trees file")
        self.trees = self.read_trees(file_path, self.uqam_codes, self.codes_data)
        self.time_now = datetime.datetime.now().strftime('%d_%m_%Y_%H_%M_%S')
        print(len(self.trees))
        print("Indexing trees")
        self.index_documents(self.trees)
        # print("Exporting trees to csv")
        # self.export_to_csv(self.trees)
        print("Generating geojson")
        self.generate_geojson(self.trees)
        print("Exporting trees to mbtiles")
        self.export_to_mbtiles()
        print("Generating colors")
        self.generate_colors(self.unique_species)

    def progressBar(self, iterable, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """
        total = len(iterable)

        # Progress Bar Printing Function
        def printProgressBar(iteration):
            percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
            filledLength = int(length * iteration // total)
            bar = fill * filledLength + '-' * (length - filledLength)
            print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)

        # Initial Call
        printProgressBar(0)
        # Update Progress Bar
        for i, item in enumerate(iterable):
            yield item
            printProgressBar(i + 1)
        # Print New Line on Complete
        print()

    def read_trees(self, file_path, uqam_code, codes_data):
        tree_id = 0
        list_trees = []
        filepath = os.path.abspath(file_path)
        files = glob.glob(filepath)
        for file in files:
            print(file)
            # filepath = os.path.abspath(file_path) ## ~ os.getcwd()
            with open(file, 'r', encoding='utf8') as csv_file:
                reader = csv.DictReader(csv_file)
                for row in self.progressBar(list(reader), prefix='Progress:', suffix='Complete', length=50):
                    tree = {}
                    # tree = row
                    tree['inventory'] = row.get("INV_TYPE", "00")
                    tree['district_id'] = row.get("ARROND", "00")
                    tree['district'] = row.get("ARROND_NOM", "00")

                    tree['street'] = row.get("Rue", "00")
                    tree['street_side'] = row.get("COTE", "00")
                    tree['civic_number'] = str(row.get("No_civique", "00"))
                    tree['location_number'] = row.get("EMP_NO", "00")
                    tree['tree_state'] = row.get("ETAT", "Inconnu")

                    tree['location_type'] = row.get("Emplacement", "00")

                    tree['specie_latin_original'] = row['Essence_latin']
                    # tree['specie_latin_cleaned2'] = self.remove_variety(row['Essence_latin'])
                    # tree['specie_latin_cleaned'] = row['specie_latin_cleaned']

                    # tree['specie_french'] = row.get("Essence_fr", "00")
                    # tree['specie_english'] = row.get("ESSENCE_ANG", "00")

                    tree['DBH'] = row['DHP']
                    tree['date_measures'] = self.convert_dates(row.get("Date_releve", "00"))
                    tree['date_plant'] = self.convert_dates(row.get("Date_plantation", "00"))
                    tree['ID_SylvCiT_code'] = row.get("ID_SylvCiT_code", "00")

                    tree['health_condition_str'] = str(row.get("cote_sante", "00"))
                    tree['health_condition_per'] = self.get_per_condition(row.get("cote_sante", "00"),
                                                                          row['Essence_latin'])
                    tree['localization'] = row.get("LOCALISATION", "00")
                    tree['park_id'] = row.get("CODE_PARC", "00")
                    tree['park_name'] = row.get("NOM_PARC", "00")
                    tree['tree_id'] = tree_id

                    uqam_codes = codes_data.latin_name.tolist()
                    best_match = [d for d in uqam_code if
                                  (d['latin_name']).lower().strip() == (row['Essence_latin']).lower().strip()]
                    # print(len(best_match))
                    # print(row['Essence_latin'])
                    if len(best_match) == 0:
                        best_match = process.extractOne((row['Essence_latin']).strip(), uqam_codes,
                                                        scorer=fuzz.token_sort_ratio)
                        if best_match[1] < 70:
                            # print(row.get("Essence_latin", "00"))
                            try:
                                first_word_species = (row.get("Essence_latin", "00")).split()[0]
                                best_match_row = codes_data.loc[codes_data['latin_name'] == first_word_species]
                            except:
                                best_match_row = codes_data.loc[codes_data['mtl_code'] == 'AAAA']

                            if len(best_match_row) == 0:
                                best_match_row = codes_data.loc[codes_data['mtl_code'] == 'AAAA']
                        else:
                            best_match_row = codes_data.loc[codes_data['latin_name'] == best_match[0]]

                    else:
                        best_match_row = codes_data.loc[
                            codes_data['latin_name'].str.lower().str.strip() == row['Essence_latin'].lower().strip()]
                    if len(best_match_row) == 0:
                        best_match_row = codes_data.loc[codes_data['mtl_code'] == 'AAAA']
                    # print(tree_id)
                    tree['uqam_code'] = best_match_row['uqam_code'].values[0]
                    tree['code'] = best_match_row['uqam_code'].values[0]
                    tree['family'] = best_match_row['family'].values[0]
                    tree['genus'] = best_match_row['genus'].values[0]
                    tree['func_group'] = best_match_row['func_group'].values[0]
                    tree['specie_latin'] = best_match_row['latin_name'].values[0]
                    tree['specie_latin_cleaned'] = (best_match_row['species_latin_cleaned'].values[0]).strip()
                    tree['specie_french'] = best_match_row['french_name'].values[0]
                    tree['specie_english'] = best_match_row['english_name'].values[0]

                    if self.validate_lat_long(row):
                        tree['coord'] = "{0},{1}".format(row['Latitude'], row['Longitude'])
                    elif self.validate_mtm(row):
                        lat, long = self.mtm_converter.to_latlon(float(row['x']), float(row['y']), int(row['mtm_zone']))
                        tree['coord'] = "{0},{1}".format(lat, long)
                    tree_id += 1
                    list_trees.append(tree)
                    self.unique_species.add(tree['specie_latin_cleaned'])
        return list_trees

    @staticmethod
    def validate_mtm(row):
        try:
            float(row["x"])
            float(row["y"])
            int(row["mtm_zone"])
            return True
        except Exception:
            return False

    @staticmethod
    def validate_lat_long(row):
        return 'Latitude' in row and 'Longitude' in row and row["Latitude"] != '' and row["Longitude"] != ''

    def convert_dates(self, dates):
        if dates == "00":
            return "00"
        else:
            try:
                retrieved_date = parser.parse(dates)
                return retrieved_date.strftime("%d/%m/%Y")
            except:
                return "00"

    def get_per_condition(self, health_condition, species_name):
        if isinstance(health_condition, str):
            if "fraxinus" in species_name.lower():
                return 0.35
            else:
                switcher = {
                    'Bonne': 0.9,
                    'Moyenne': 0.8,
                    'Faible': 0.6,
                    'Mauvaise': 0.25,
                    'Mort': 0
                }
                response = switcher.get(health_condition, health_condition)
        else:
            response = health_condition
        return response

    def remove_variety(self, species):
        if species == "Amelanchier c. 'Rainbow Pillar' (Glenn Form)":
            species = 'Amelanchier canadensis'
        if species == "Acer g. Flame tiges multiples":
            species = 'Acer ginnala'
        if species == "Gymnocladus d.  (PrairieTitanTM)":
            species = 'Gymnocladus Dioicus'
        if species == "Populus sp. (hybrides)":
            species = 'Populus x'
        if species == "Tilia a. 'Lincoln'":
            species = 'Tilia americana'
        if species == "Fraxinus pensylvanica":
            species = 'Fraxinus pennsylvanica'
        if species == "Acer sacccharum'":
            species = 'Acer saccharum'
        if species == "Acer x freemanii":
            species = 'Acer freemanii'
        if species == "Pseudostuga sp.":
            species = "Pseudotsuga sp."
        if species == "Pyrus multifruits":
            species = "Pyrus sp."
        if species == "Prunus multifruits":
            species = "Prunus sp."
        # Removes string between ''
        without_variety = re.sub(r" '(.*)'", "", species, flags=re.IGNORECASE)
        without_par = re.sub(r' (\(.*\))', '', without_variety, flags=re.IGNORECASE)
        # Removes all text begining with var
        without_var = re.sub(r" var(.*)", "", without_par, flags=re.IGNORECASE)
        # Removes all text after x (hybrides), we keep only Acer x freemanii
        without_hybrides = without_var
        if without_hybrides.lower() != 'acer x freemanii'.lower():
            without_hybrides = re.sub(r"(?i) x(.*)", " x", without_hybrides, flags=re.IGNORECASE)
            # Keep only the two first words
            without_hybrides = ' '.join(without_hybrides.split()[:2])

        if without_hybrides == "Acer freemanii":
            without_hybrides = "Acer x freemanii"
        if without_hybrides == "Amelanchier g.":
            without_hybrides = "Amelanchier x"
        if without_hybrides == "Populus sp.(hybrides)":
            without_hybrides = "Populus x"
        if without_hybrides == "Thuya occidentalis":
            without_hybrides = "Thuja occidentalis"
        if without_hybrides == "Tilia a.":
            without_hybrides = "Tilia americana"
        if without_hybrides == "Prunus maakii":
            without_hybrides = "Prunus maackii"
        if without_hybrides == "Picea punges":
            without_hybrides = "Picea pungens"
        if without_hybrides == "Acer sacccharum":
            without_hybrides = 'Acer saccharum'
        if without_hybrides == "Acer tatricum":
            without_hybrides = 'Acer tataricum'
        if without_hybrides == "Caragana arobrescens":
            without_hybrides = 'Caragana arborescens'
        if without_hybrides == "Amelanchier 'Autumn'":
            without_hybrides = "Amelanchier x"
        if without_hybrides == "Cercidiphyllym japonicum":
            without_hybrides = "Cercidiphyllum japonicum"
        if without_hybrides == "Malux x":
            without_hybrides = "Malus x"
        if without_hybrides == "Prunus 'ceriser":
            without_hybrides = "Prunus sp."
        if without_hybrides == "Prunus sub.":
            without_hybrides = "Prunus sp."
        if without_hybrides == "Pseudostuga menziesii":
            without_hybrides = "Pseudotsuga menziesii"
        if without_hybrides == "Pyrus calleryanas":
            without_hybrides = "Pyrus calleryana"
        if without_hybrides == "Ulmus a.":
            without_hybrides = "Ulmus americana"
        if without_hybrides == "Ulmus 13-0053":
            without_hybrides = "Ulmus x"
        if without_hybrides == "Aesculus arguta":
            without_hybrides = "Aesculus glabra"
        if without_hybrides == "Aesculus carnea":
            without_hybrides = "Aesculus x"
        if without_hybrides == "Aesculus octandra":
            without_hybrides = "Aesculus flava"
        if without_hybrides == "Cladastris kentukea":
            without_hybrides = "Aesculus lutea"
        if without_hybrides == "Gleditsia t.":
            without_hybrides = "Gleditsia triacanthos"
        if without_hybrides == "Phellodendron l.":
            without_hybrides = "Phellondendron lavallei"
        if without_hybrides == "Populus canescens":
            without_hybrides = "Populus x"
        if without_hybrides == "Ulmus davidiana":
            without_hybrides = "Ulmus x"

        if without_hybrides.strip().count(' ') == 0:
            without_hybrides = without_hybrides + ' ' + 'sp.'
        return without_hybrides

    def read_uqam_code(self, file_path):
        filepath = os.path.abspath(file_path)
        with open(file_path, encoding='utf8') as f:
            a = [{k: v for k, v in row.items()}
                 for row in csv.DictReader(f, skipinitialspace=True, delimiter=';')]
        return a

    # Yield successive n-sized 
    # chunks from l. 
    def divide_chunks(self, docs, batch_size):
        # looping till length l 
        for i in range(0, len(docs), batch_size):
            yield docs[i:i + batch_size]

    def index_documents(self, documents):
        solr = pysolr.Solr("{0}:{1}/solr/{2}".format(
            "http://localhost",
            "8984",
            "urban-forest"
        ), always_commit=True, timeout=int(10000))
        solr.delete(q='*:*')
        batch_size = 10000
        for chunk in self.progressBar(list(self.divide_chunks(documents, batch_size)), prefix='Progress:',
                                      suffix='Complete', length=50):
            solr.add(chunk)
            # print("Indexing {0} documents".format(len(chunk)))
        print("Indexed {0} documents".format(len(documents)))

    def generate_geojson(self, documents):
        template = \
            ''' \
           { "type" : "Feature",
               "geometry" : {
                   "type" : "Point",
                   "coordinates" : [%s, %s]},
               "properties" : { "species_latin" : "%s", "DBH" : "%s", "tree_id": "%s"}
               },
           '''
        template_match = \
            ''' \
           ["%s"],
          "%s",
        '''
        # the head of the geojson file
        output = \
            ''' \

        { "type" : "Feature Collection",
           "features" : [
           '''
        # tree_id = 0
        for document in self.progressBar(documents, prefix='Progress:', suffix='Complete', length=50):
            # the template. where data from the csv will be formatted to geojson
            species_latin = document["specie_latin_cleaned"]
            tree_id = document["tree_id"]
            if "coord" in document and len(document["DBH"]) > 0:
                DBH = document["DBH"]
                lat = (document["coord"]).split(",")[0]
                lon = (document["coord"]).split(",")[1]
                output += template % (lon, lat, species_latin, DBH, tree_id)
            # tree_id += 1

        # the tail of the geojson file
        output += \
            ''' \
           ]

        }
           '''

        # opens an geoJSON file to write the output
        # outFileHandle = open('./outputs/latest_mtl_trees_'+self.time_now+'.geojson', "w")
        outFileHandle = open('./outputs/latest_mtl_trees.geojson', "w", encoding='utf8')
        outFileHandle.write(output)
        outFileHandle.close()

    def generate_colors(self, species):
        print("Number of species")
        print(len(species))
        template_match = \
            ''' \
        ["%s"],
        "%s",
        '''
        # the head of the geojson file
        output = \
            ''' \

        { "type" : "Feature Collection",
        "features" : [
        '''
        colors = \
            ''' \
        [
        "match",
        ["get", "species_latin"],
        '''
        for species_name in self.progressBar(species, prefix='Progress:', suffix='Complete', length=50):
            # the template. where data from the csv will be formatted to geojson
            # species_name = document["specie_latin_cleaned"]
            # print(species_name)
            # colors+=template_match % (species_name, (ColorHash(species_name).hex))
            hash_color = hash(species_name)
            r = (hash_color & 0xFF0000) >> 16;
            g = (hash_color & 0x00FF00) >> 8;
            b = hash_color & 0x0000FF;
            colors += template_match % (species_name, '#%02x%02x%02x' % (r, g, b))

        colors += \
            ''' \
        "#000000"
        ]
        '''

        outFileHandle2 = open("colors" + self.time_now + ".json", "w", encoding='utf8')
        outFileHandle2.write(colors)
        outFileHandle2.close()

    def export_to_csv(self, documents):
        print(type(documents))
        print(documents[0])
        keys = documents[0].keys()
        with open('./outputs/montreal_trees_' + self.time_now + '.csv', 'w', encoding='utf8',
                  newline='') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(documents)

    def export_to_mbtiles(self):
        with open("output.log", "a") as output:
            subprocess.call(
                "docker exec -it urbanforest_tippecanoe tippecanoe -o /data/mtl_trees" + self.time_now + ".mbtiles /data/latest_mtl_trees.geojson --layer='mtl-trees' --base-zoom=10 --force --maximum-zoom=15 --minimum-zoom=9 --extend-zooms-if-still-dropping --increase-gamma-as-needed",
                shell=True)


# parserIndexer =  ParserIndexer('../data/trees_data/arbres-publics.csv')
parserIndexer = ParserIndexer('./data_index/*.csv')
