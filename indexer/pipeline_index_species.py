import json
import os, os.path
import pysolr
import csv
import re

documents = []

class ParserIndexer:
    def __init__(self, file_path) :
        self.trees = self.read_species(file_path)
        print(len(self.trees))
        self.index_documents(self.trees)
        # self.export_to_csv(self.trees)

    def read_species(self, file_path):
        list_trees = []
        filepath = os.path.abspath(file_path) ## ~ os.getcwd()
        with open(filepath, 'r', encoding='utf8') as csv_file:
            reader = csv.DictReader(csv_file)
            tree_id = 0
            for row in reader:
                tree = {}
                tree = row
                try:
                    tree['new_max_DBH'] =  round(float(row['new_max_dbh']), 2)
                except ValueError:
                    print(row['new_max_dbh'])
                list_trees.append(tree)
            return list_trees


    # Yield successive n-sized 
    # chunks from l. 
    def divide_chunks(self, docs, batch_size): 
        # looping till length l 
        for i in range(0, len(docs), batch_size):  
            yield docs[i:i + batch_size]
    
    def index_documents(self, documents):
        solr = pysolr.Solr("{0}:{1}/solr/{2}".format(
            "http://localhost",
            "8984",
            "species"
        ), always_commit=True, timeout = int(10000))
        solr.delete(q='*:*')
        batch_size = 10000
        for chunk in list(self.divide_chunks(documents, batch_size)):
            solr.add(chunk)
            print("Indexing {0} documents".format(len(chunk)))
        print("Indexed {0} documents".format(len(documents)))

    def export_to_csv(self, documents):
        print(type(documents))
        print(documents[0])
        keys = documents[0].keys()
        with open('species.csv', 'w',encoding='utf8', newline='')  as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(documents)
        
        

parserIndexer =  ParserIndexer('./species_type_new_maxdbh.csv')