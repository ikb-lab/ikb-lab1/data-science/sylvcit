from mapbox import Uploader
from time import sleep
from random import randint

service = Uploader()

mapid = getfixture('uploads_dest_id') # 'uploads-test'
with open('tests/twopoints.geojson', 'rb') as src:
	upload_resp = service.upload(src, mapid)

if upload_resp.status_code == 422:
    for i in range(5):
       sleep(5)
       with open('tests/twopoints.geojson', 'rb') as src:
            upload_resp = service.upload(src, mapid)
       if upload_resp.status_code != 422:
           break