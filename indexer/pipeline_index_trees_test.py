import json
import os, os.path
import pysolr
import csv
import re
import datetime
import pandas
import glob
import subprocess
from fuzzywuzzy import fuzz
from fuzzywuzzy import process


documents = []

class ParserIndexer:
    def __init__(self, file_path) :
        self.uqam_codes = self.read_uqam_code('../data/uqam_codes.csv')
        self.codes_data = pandas.read_csv('../data/uqam_codes.csv')
        print("Reading and formatting trees file")
        self.trees = self.read_trees(file_path, self.uqam_codes, self.codes_data)
        self.time_now  = datetime.datetime.now().strftime('%d_%m_%Y_%H_%M_%S') 
        print(len(self.trees))
        # print("Indexing trees")
        # self.index_documents(self.trees)

        print("Exporting trees to csv")
        self.export_to_csv(self.trees)
        # print("Generating geojson")
        # self.generate_geojson(self.trees)
        # print("Exporting trees to mbtiles")
        # self.export_to_mbtiles()
    
    def progressBar(self, iterable, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """
        total = len(iterable)
        # Progress Bar Printing Function
        def printProgressBar (iteration):
            percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
            filledLength = int(length * iteration // total)
            bar = fill * filledLength + '-' * (length - filledLength)
            print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
        # Initial Call
        printProgressBar(0)
        # Update Progress Bar
        for i, item in enumerate(iterable):
            yield item
            printProgressBar(i + 1)
        # Print New Line on Complete
        print()

    def read_trees(self, file_path, uqam_code, codes_data):
        tree_id = 0
        list_trees = []
        filepath = os.path.abspath(file_path) 
        files=glob.glob(filepath)   
        for file in files: 
            print(file)
            # filepath = os.path.abspath(file_path) ## ~ os.getcwd()
            with open(file, 'r', encoding='utf8') as csv_file:
                reader = csv.DictReader(csv_file)
                for row in reader:
                    tree = {}
                    # tree = row
                    # tree['inventory'] = row.get("INV_TYPE", "00")
                    # tree['district_id'] = row.get("ARROND", "00")
                    # tree['district'] = row.get("ARROND_NOM", "00")
                    
                    # tree['street'] = row.get("Rue", "00")
                    # tree['street_side'] = row.get("COTE", "00")
                    # tree['civic_number'] = row.get("No_civique", "00")
                    # tree['location_number'] = row.get("EMP_NO", "00")
                    
                    # tree['location_type'] = row.get("Emplacement", "00")

                    
                    tree['specie_latin'] = row['Essence_latin']
                    tree['specie_latin_cleaned'] = self.remove_variety(row['Essence_latin'])
                    # tree['specie_latin_cleaned'] = row['specie_latin_cleaned']

                    tree['specie_french'] = row.get("Essence_fr", "00")
                    tree['specie_english'] = row.get("ESSENCE_ANG", "00")

                    # tree['DBH'] = row['DHP']
                    # tree['date_measures'] = row.get("Date_releve", "00")
                    # tree['date_plant'] = row.get("Date_plantation", "00")
                    # tree['health_condition_str'] = row.get("cote_sante", "00")
                    # tree['health_condition_per'] = self.get_per_condition(row.get("cote_sante", "00"), row['Essence_latin'])


                    # expectedResult = [d for d in uqam_code if d['code_uqam'] == row['SIGLE']]
                    # if len(expectedResult) == 0:
                    #     expectedResult = [d for d in uqam_code if d['mtl_code'] == 'AAAA']

                    # tree['uqam_code'] = expectedResult[0]['uqam_code']
                    # tree['code'] = expectedResult[0]['uqam_code']
                    # tree['family'] = expectedResult[0]['family']
                    # tree['genus'] = expectedResult[0]['genre']
                    # tree['func_group'] = expectedResult[0]['func_group']
                    uqam_codes = codes_data.latin_name.tolist()
                    best_match = [d for d in uqam_code if (d['latin_name']).lower() == (row['Essence_latin']).lower()]
                    if len(best_match) == 0:
                        best_match_simple_ratio = process.extractOne(row['Essence_latin'], uqam_codes)
                        best_match_token_sort_ratio = process.extractOne(row['Essence_latin'], uqam_codes, scorer=fuzz.token_sort_ratio)
                        best_match_token_set_ratio = process.extractOne(row['Essence_latin'], uqam_codes, scorer=fuzz.token_set_ratio)
                        
                        # print('row Essence_latin : ', row['Essence_latin'])
                        # print('best match : ', best_match_token_sort_ratio)
                        best_match_row_simple_ratio = codes_data.loc[codes_data['latin_name'] == best_match_simple_ratio[0]]
                        best_match_row_token_sort_ratio = codes_data.loc[codes_data['latin_name'] == best_match_token_sort_ratio[0]]
                        best_match_row_token_set_ratio = codes_data.loc[codes_data['latin_name'] == best_match_token_set_ratio[0]]
                        tree['matching_score_simple_ratio'] = best_match_simple_ratio[1]
                        tree['matching_score_token_sort_ratio'] = best_match_token_sort_ratio[1]
                        tree['matching_score_token_set_ratio'] = best_match_token_set_ratio[1]

                        tree['sigle_mtl'] = row.get("SIGLE", "00")
                        tree['specie_latin_uqam_code_simple_ratio'] = best_match_row_simple_ratio['latin_name'].values[0]
                        tree['specie_latin_uqam_code_token_sort_ratio'] = best_match_row_token_sort_ratio['latin_name'].values[0]
                        tree['specie_latin_uqam_code_token_set_ratio'] = best_match_row_token_set_ratio['latin_name'].values[0]
                        if best_match_token_sort_ratio[1] < 70:
                            first_word_species = (row['Essence_latin']).split()[0]
                            species_match = codes_data.loc[codes_data['latin_name'] == first_word_species]
                            # species_match = [d for d in uqam_code if (d['latin_name']).lower() == first_word_species.lower()]
                            if len(species_match) != 0:
                                # best_match_species_row = species_match[0]
                                tree['specie_latin_uqam_code_token_sort_ratio'] = species_match['latin_name'].values[0]
                            else :
                                species_match = codes_data.loc[codes_data['mtl_code'] == 'AAAA']
                                # unknown_row = [d for d in uqam_code if d['mtl_code'] == 'AAAA']
                                # unknown_species_row = unknown_row[0]
                                tree['specie_latin_uqam_code_token_sort_ratio'] = species_match['latin_name'].values[0]
                        list_trees.append(tree)
                        # print('Sigle mtl : ', row['SIGLE'])
                        # print(best_match_row)
                    # else :
                    #     # best_match_row = best_match[0]
                    #     best_match_row = codes_data.loc[codes_data['latin_name'] == row['Essence_latin']]
                    #     tree['matching_score'] = 1000000
                    #     tree['sigle_mtl'] = row.get("SIGLE", "00")
                    #     tree['specie_latin_uqam_code'] = best_match_row['latin_name'].values[0]
                    # else:
                    #     best_match_row = [d for d in uqam_code if d['mtl_code'] == 'AAAA'][0]


                    # tree['specie_latin_cleaned_uqam_code'] = best_match_row['species_latin_cleaned'].values[0]
                    # tree['ID_SylvCiT_code'] = best_match_row['ID_SylvCiT_code'].values[0]
                    # tree['uqam_code'] = best_match_row['uqam_code'].values[0]
                    # tree['family'] = best_match_row['family'].values[0]
                    # tree['genus'] = best_match_row['genre'].values[0]
                    # tree['func_group'] = best_match_row['func_group'].values[0]
                    # if row['Latitude'] != '' and row['Longitude'] != '' :
                    #     tree['coord'] = "{0},{1}".format(row['Latitude'] , row['Longitude'])
                    tree_id += 1
                    
        return list_trees

    def get_per_condition(self, health_condition, species_name):
        if "fraxinus" in species_name.lower():
            return 0.35
        else:
            switcher={
                    'Bonne':0.9,
                    'Moyenne':0.8,
                    'Faible':0.6,
                    'Mauvaise':0.25,
                    'Mort':0
                 }
        return switcher.get(health_condition, 0.75)


    def remove_variety(self, species):
        if species == "Amelanchier c. 'Rainbow Pillar' (Glenn Form)":
            species = 'Amelanchier canadensis'
        if species == "Acer g. Flame tiges multiples":
            species = 'Acer ginnala'
        if species == "Gymnocladus d.  (PrairieTitanTM)":
            species = 'Gymnocladus Dioicus'
        if species == "Populus sp. (hybrides)":
            species = 'Populus x'
        if species == "Tilia a. 'Lincoln'":
            species = 'Tilia americana'
        if species =="Fraxinus pensylvanica":
            species = 'Fraxinus pennsylvanica'
        if species == "Acer sacccharum'":
            species = 'Acer saccharum'
        if species == "Acer x freemanii":
            species = 'Acer freemanii'
        if species == "Pseudostuga sp.":
            species = "Pseudotsuga sp."
        if species == "Pyrus multifruits":
            species = "Pyrus sp."
        if species == "Prunus multifruits":
            species = "Prunus sp."
        # Removes string between ''
        without_variety = re.sub(r" '(.*)'", "", species, flags=re.IGNORECASE)
        without_par = re.sub(r' (\(.*\))','',without_variety, flags=re.IGNORECASE)
        # Removes all text begining with var
        without_var = re.sub(r" var(.*)", "", without_par, flags=re.IGNORECASE)
        # Removes all text after x (hybrides), we keep only Acer x freemanii
        without_hybrides = without_var
        if without_hybrides.lower() != 'acer x freemanii'.lower():
            without_hybrides = re.sub(r"(?i) x(.*)", " x", without_hybrides, flags=re.IGNORECASE)
            # Keep only the two first words
            without_hybrides =' '.join(without_hybrides.split()[:2])

        if without_hybrides == "Acer freemanii":
            without_hybrides = "Acer x freemanii"
        if without_hybrides == "Amelanchier g.":
            without_hybrides = "Amelanchier x"
        if without_hybrides == "Populus sp.(hybrides)":
            without_hybrides = "Populus x"
        if without_hybrides == "Thuya occidentalis":
            without_hybrides = "Thuja occidentalis"
        if without_hybrides == "Tilia a.":
            without_hybrides = "Tilia americana"
        if without_hybrides == "Prunus maakii":
            without_hybrides = "Prunus maackii"
        if without_hybrides == "Picea punges":
            without_hybrides = "Picea pungens"
        if without_hybrides == "Acer sacccharum":
            without_hybrides = 'Acer saccharum'
        if without_hybrides == "Acer tatricum":
            without_hybrides = 'Acer tataricum'
        if without_hybrides == "Caragana arobrescens":
            without_hybrides = 'Caragana arborescens'
        if without_hybrides == "Amelanchier 'Autumn'":
        	without_hybrides = "Amelanchier x"
        if without_hybrides == "Cercidiphyllym japonicum":
        	without_hybrides = "Cercidiphyllum japonicum"
        if without_hybrides == "Malux x":
        	without_hybrides = "Malus x"
        if without_hybrides == "Prunus 'ceriser":
            without_hybrides = "Prunus sp."    
        if without_hybrides == "Prunus sub.":
            without_hybrides = "Prunus sp."   
        if without_hybrides == "Pseudostuga menziesii":
            without_hybrides = "Pseudotsuga menziesii"   
        if without_hybrides == "Pyrus calleryanas":
            without_hybrides = "Pyrus calleryana"   
        if without_hybrides == "Ulmus a.":
            without_hybrides = "Ulmus americana"   
        if without_hybrides == "Ulmus 13-0053":
            without_hybrides = "Ulmus x"  
        if without_hybrides == "Aesculus arguta":
            without_hybrides = "Aesculus glabra"
        if without_hybrides == "Aesculus carnea":
            without_hybrides = "Aesculus x"
        if without_hybrides == "Aesculus octandra":
            without_hybrides = "Aesculus flava"
        if without_hybrides == "Cladastris kentukea":
            without_hybrides = "Aesculus lutea"        
        if without_hybrides == "Gleditsia t.":
            without_hybrides = "Gleditsia triacanthos"      
        if without_hybrides == "Phellodendron l.":
            without_hybrides = "Phellondendron lavallei"
        if without_hybrides == "Populus canescens":
            without_hybrides = "Populus x"
        if without_hybrides == "Ulmus davidiana":
            without_hybrides = "Ulmus x"

            
        if without_hybrides.strip().count(' ')==0:
            without_hybrides = without_hybrides + ' ' + 'sp.'
        return without_hybrides

        
    def read_uqam_code(self, file_path):
        filepath = os.path.abspath(file_path)
        with open(file_path, encoding='utf8') as f:
            a = [{k: v for k, v in row.items()}
            for row in csv.DictReader(f, skipinitialspace=True)]
        return a
    
    # Yield successive n-sized 
    # chunks from l. 
    def divide_chunks(self, docs, batch_size): 
        # looping till length l 
        for i in range(0, len(docs), batch_size):  
            yield docs[i:i + batch_size]
    
    def index_documents(self, documents):
        solr = pysolr.Solr("{0}:{1}/solr/{2}".format(
            "http://localhost",
            "8984",
            "urban-forest"
        ), always_commit=True, timeout = int(10000))
        solr.delete(q='*:*')
        batch_size = 10000
        for chunk in self.progressBar(list(self.divide_chunks(documents, batch_size)), prefix = 'Progress:', suffix = 'Complete', length = 50):
            solr.add(chunk)
            # print("Indexing {0} documents".format(len(chunk)))
        print("Indexed {0} documents".format(len(documents)))

    def generate_geojson(self, documents):
        template = \
           ''' \
           { "type" : "Feature",
               "geometry" : {
                   "type" : "Point",
                   "coordinates" : [%s, %s]},
               "properties" : { "species_latin" : "%s", "DBH" : "%s", "tree_id": "%s"}
               },
           '''

        species=set()
        template_match = \
           ''' \
           ["%s"],
          "%s",
        '''
        # the head of the geojson file
        output = \
           ''' \

        { "type" : "Feature Collection",
           "features" : [
           '''
        # tree_id = 0
        for document in self.progressBar(documents, prefix = 'Progress:', suffix = 'Complete', length = 50):
            # the template. where data from the csv will be formatted to geojson
            species_latin = document["specie_latin_cleaned"]
            tree_id = document["tree_id"]
            if "coord" in document and len(document["DBH"])>0 :
                DBH = document["DBH"]
                lat = (document["coord"]).split(",")[0]
                lon = (document["coord"]).split(",")[1]
                output += template % (lon, lat, species_latin,DBH, tree_id)
            # tree_id += 1

        # the tail of the geojson file
        output += \
           ''' \
           ]

        }
           '''

        #opens an geoJSON file to write the output
        # outFileHandle = open('./outputs/latest_mtl_trees_'+self.time_now+'.geojson', "w")
        outFileHandle = open('./outputs/latest_mtl_trees.geojson', "w")
        outFileHandle.write(output)
        outFileHandle.close()
        


    def export_to_csv(self, documents):
        print(type(documents))
        print(documents[0])
        keys = documents[0].keys()
        with open('./outputs/montreal_trees_'+self.time_now+'.csv', 'w',encoding='utf8', newline='')  as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(documents)

        # batch_size = 10000
        # _batch = []
        # for solr_document in documents:
        #     _batch.append(solr_document)
        #     if len(_batch) == int(batch_size):
        #         solr.add(_batch)
        #         print("Indexing {0} documents".format(batch_size))
        #         _batch = []
        # print("Indexed {0} documents".format(len(documents)))
    
    def export_to_mbtiles(self):
        with open("output.log", "a") as output:
            subprocess.call("docker exec -it urbanforest_tippecanoe tippecanoe -o /data/mtl_trees.mbtiles /data/latest_mtl_trees.geojson --layer='mtl-trees' --base-zoom=10 --force --maximum-zoom=15 --minimum-zoom=9 --extend-zooms-if-still-dropping --increase-gamma-as-needed", shell=True)
        

# parserIndexer =  ParserIndexer('../data/trees_data/arbres-publics.csv')
parserIndexer =  ParserIndexer('./data_index/*.csv')