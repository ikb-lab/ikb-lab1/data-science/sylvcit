## Running the project

1. Be sure that you installed docker and docker-compose
2. Run 'docker-compose up --build'
3. Enjoy.



## Table of contents
* [General info](#general-info)
* [Prerequisites](#Prerequisites)
* [Installation](#Installation)
* [License](#License)

## General info
SylvCiT is an artificial intelligence tool designed to optimize the resilience of urban and peri-urban forests in North America.

It aims to provide practical recommendations and analysis to improve the health, diversity and ecosystem services provided by urban forest.

### Prerequisites

Please note that the software provided was developed and run on Ubuntu 18.4 systems with Python 3.6.
While Python can run on a variety of systems, these instructions are written for the aforementioned specifications.


### Installation

```bash
$ git clone http://gitlab.ikb.info.uqam.ca/ikb-lab/data-science/sylvcit.git
$ cd sylvcit;
$ docker-compose up -d;
```

### License
All code found in this repository is licensed under GPL v3.
